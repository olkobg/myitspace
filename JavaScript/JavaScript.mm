<map version="1.1.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1283935270578" ID="ID_332869610" LINK="../Index.mm" MODIFIED="1481019674451" TEXT="JavaScript">
<font NAME="SansSerif" SIZE="22"/>
<node CREATED="1283935276328" FOLDED="true" ID="ID_1046873276" MODIFIED="1481393915324" POSITION="right" TEXT="app/lib/tool">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1480677503662" FOLDED="true" ID="ID_301374293" MODIFIED="1480960263459" TEXT="general">
<node COLOR="#0033ff" CREATED="1290773593462" FOLDED="true" ID="ID_417996470" LINK="jQuery.mm" MODIFIED="1480681079884" TEXT="jQuery">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1429191781737" FOLDED="true" ID="ID_1421355922" MODIFIED="1480681078804" TEXT="ui">
<node COLOR="#0033ff" CREATED="1354015782893" ID="ID_517049765" LINK="jquery/jqueryUI.mm" MODIFIED="1429191776486" TEXT="jqueryUI">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1429191784755" FOLDED="true" ID="ID_694085912" MODIFIED="1480681078174" TEXT="slideshow">
<node COLOR="#0033ff" CREATED="1429191792320" ID="ID_1008847364" LINK="http://www.skitter-slider.net/" MODIFIED="1429191806467" TEXT="skitter-slider"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1356445908888" ID="ID_187919146" LINK="http://amplifyjs.com/" MODIFIED="1429191776486" TEXT="apmplify">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1288878939472" ID="ID_626818632" LINK="http://mootools.net/" MODIFIED="1420822058433" TEXT="mootolls">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1294140297055" ID="ID_1539605617" LINK="prototype.mm" MODIFIED="1420822058433" TEXT="prototype">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1283935280234" FOLDED="true" ID="ID_569325362" LINK="http://script.aculo.us/" MODIFIED="1420822058432" TEXT="script.aculo.us">
<font NAME="SansSerif" SIZE="13"/>
<node CREATED="1283935288203" ID="ID_310409909" LINK="http://wiki.github.com/madrobby/scriptaculous/" MODIFIED="1307097825170" TEXT="effects"/>
</node>
</node>
<node CREATED="1480677509844" FOLDED="true" ID="ID_901246470" MODIFIED="1481038085110" TEXT="utils">
<node COLOR="#0033ff" CREATED="1375971244809" ID="ID_852111777" LINK="http://underscorejs.org/" MODIFIED="1443267265661" TEXT="undrscore.js">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1480677569807" ID="ID_498342920" MODIFIED="1480681071761" TEXT="lodash"/>
</node>
<node CREATED="1363789987681" FOLDED="true" ID="ID_1552324385" MODIFIED="1481393914851" TEXT="mvvc">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#338800" CREATED="1417646209005" FOLDED="true" ID="ID_464220657" LINK="Framewrk/angular.mm" MODIFIED="1481393914379" TEXT="angular.js">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1418753764713" ID="ID_740836588" LINK="http://angular-ui.github.io/" MODIFIED="1480677102677" TEXT="angular-ui">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1423308841622" ID="ID_671061884" LINK="http://angular-ui.github.io/bootstrap/" MODIFIED="1423308854035" TEXT="UI Bootstrap"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480677106502" ID="ID_807481474" LINK="https://angular.io/" MODIFIED="1480678285323" TEXT="angular2"/>
<node COLOR="#0033ff" CREATED="1375972457479" ID="ID_295083454" LINK="http://emberjs.com/" MODIFIED="1420822058433" TEXT="ember.js">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#338800" CREATED="1443266506388" ID="ID_973811723" LINK="Framewrk/backbonejs.mm" MODIFIED="1481038012636" TEXT="backbone.js"/>
<node COLOR="#338800" CREATED="1445771532303" ID="ID_758167572" LINK="Framewrk/reactjs.mm" MODIFIED="1481038009914" TEXT="react.js"/>
<node COLOR="#0033ff" CREATED="1363789995060" ID="ID_355465255" LINK="http://developer.yahoo.com/cocktails/mojito/" MODIFIED="1480960252166">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>yahoo</b>&#160;mojito
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1418753592653" FOLDED="true" ID="ID_1843180742" MODIFIED="1480960266969" TEXT="build-tools">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1377210482197" ID="ID_1445245110" LINK="http://gruntjs.com/" MODIFIED="1420839914744" TEXT="grunt">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1443689513544" ID="ID_1655232580" LINK="http://gulpjs.com/" MODIFIED="1443689696415" TEXT="gulp"/>
<node COLOR="#0033ff" CREATED="1480676733804" ID="ID_138513199" LINK="https://webpack.github.io/" MODIFIED="1480677068296" TEXT="webpack"/>
</node>
<node COLOR="#0033ff" CREATED="1307097847126" FOLDED="true" ID="ID_202185443" LINK="http://en.wikipedia.org/wiki/List_of_ECMAScript_engines" MODIFIED="1480681053380" TEXT="Engine">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1357148058475" ID="ID_290312899" LINK="server/Node.js.mm" MODIFIED="1443267351327" TEXT="Node.js">
<font NAME="SansSerif" SIZE="13"/>
</node>
<node CREATED="1392649798263" ID="ID_198983299" LINK="http://phantomjs.org/" MODIFIED="1436179343835">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0000ff">phantomJS</font>&#xa0;- webkit on console
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1364305444132" FOLDED="true" ID="ID_103084548" MODIFIED="1480676726221" TEXT="mozilla">
<node COLOR="#0033ff" CREATED="1307097864888" ID="ID_722061965" LINK="https://developer.mozilla.org/en/SpiderMonkey" MODIFIED="1436179349905" TEXT="SpiderMonkey"/>
<node COLOR="#0033ff" CREATED="1307097896172" ID="ID_604037639" LINK="http://www.mozilla.org/rhino/" MODIFIED="1436179350824" TEXT="rhino"/>
</node>
</node>
<node CREATED="1337155609990" FOLDED="true" ID="ID_1410910272" MODIFIED="1480960291202" TEXT="ui">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1306946336508" ID="ID_1625499138" MODIFIED="1480960273875" TEXT="tips">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1306946338796" ID="ID_986855513" LINK="http://projects.nickstakenburg.com/tipped" MODIFIED="1420839951057" TEXT="tipped">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1418309624158" ID="ID_1738488515" MODIFIED="1480960272705" TEXT="select element">
<node COLOR="#0033ff" CREATED="1418309636840" ID="ID_197621139" LINK="https://github.com/vestman/Select-or-Die" MODIFIED="1420839957316" TEXT="select or die"/>
</node>
<node CREATED="1480960279681" FOLDED="true" ID="ID_1466808702" MODIFIED="1480960290887" TEXT="progress">
<node COLOR="#0033ff" CREATED="1480960285126" ID="ID_229451811" LINK="http://github.hubspot.com/pace/" MODIFIED="1480960288344" TEXT="pace"/>
</node>
</node>
<node CREATED="1306262117748" FOLDED="true" ID="ID_989123574" MODIFIED="1480677576535" TEXT="security">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1306262123782" ID="ID_274024630" LINK="http://www-cs-students.stanford.edu/~tjw/jsbn/" MODIFIED="1357727505063">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#009999"><b>Stanford:</b></font>&#xa0;RSA algorithm
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1418381257753" FOLDED="true" ID="ID_753511311" MODIFIED="1481019668414" TEXT="date">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1418381261261" ID="ID_584760059" LINK="http://momentjs.com/" MODIFIED="1420839942155" TEXT="http://momentjs.com"/>
</node>
<node CREATED="1290785690168" FOLDED="true" ID="ID_1371837990" MODIFIED="1480677543820" TEXT="packager">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1290785699124" ID="ID_1468703957" LINK="http://developer.yahoo.com/yui/compressor/" MODIFIED="1443689773879" TEXT="YUI-Cpmpressor"/>
<node COLOR="#0033ff" CREATED="1306946614455" ID="ID_769234349" LINK="http://code.google.com/closure/" MODIFIED="1443689773878" TEXT="Google closure tools"/>
<node COLOR="#0033ff" CREATED="1306946557807" ID="ID_1154117622" LINK="http://dean.edwards.name/packer/" MODIFIED="1443689773873" TEXT="PACKER"/>
</node>
<node CREATED="1418754196199" FOLDED="true" ID="ID_777050236" MODIFIED="1480677589608" TEXT="media">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1418754187356" FOLDED="true" ID="ID_1445648712" MODIFIED="1480677585378" TEXT="video">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1328630282633" ID="ID_102365652" LINK="http://videojs.com/" MODIFIED="1418754340682">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0000ff">video.js</font>&#xa0;- html5 video player
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1418338373914" ID="ID_1723593345" LINK="https://tokbox.com/opentok/" MODIFIED="1418754384356">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0000ff">open tok</font>&#xa0;- video translation into web
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1418753670872" FOLDED="true" ID="ID_795753681" MODIFIED="1480677589248" TEXT="graphics">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1354698019491" FOLDED="true" ID="ID_1626793799" MODIFIED="1480677588551" TEXT="svg">
<font NAME="SansSerif" SIZE="17"/>
<node COLOR="#0033ff" CREATED="1354698058675" ID="ID_863153170" LINK="https://developer.mozilla.org/en/docs/SVG_In_HTML_Introduction" MODIFIED="1443267299388" TEXT="MDN">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#0033ff" CREATED="1354698089437" ID="ID_1628290940" LINK="http://srufaculty.sru.edu/david.dailey/svg/" MODIFIED="1443267299395" TEXT="David Dailey">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1354868472981" ID="ID_419707493" LINK="http://tutorials.jenkov.com/svg/svg-and-css.html" MODIFIED="1443267299396" TEXT="svg + css">
<font NAME="SansSerif" SIZE="13"/>
</node>
<node COLOR="#0033ff" CREATED="1417439357178" ID="ID_1636332576" LINK="http://raphaeljs.com/" MODIFIED="1443267312235" TEXT="raphael.js">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1418748310818" ID="ID_1614645740" LINK="http://svgjs.com/" MODIFIED="1443267293421" TEXT="svg.js">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1419089571753" FOLDED="true" ID="ID_1287669999" MODIFIED="1480677590733" TEXT="i18n">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1419089579614" ID="ID_1198874951" LINK="https://github.com/fnando/i18n-js" MODIFIED="1480676692605" TEXT="github i18n-js + rails"/>
</node>
<node CREATED="1481038088575" FOLDED="true" ID="ID_425670705" MODIFIED="1481110879006" TEXT="type">
<node COLOR="#0033ff" CREATED="1481038092535" ID="ID_1994563885" LINK="https://flowtype.org/" MODIFIED="1481038097800" TEXT="flow"/>
</node>
<node CREATED="1481110879365" FOLDED="true" ID="ID_733615070" MODIFIED="1481110891560" TEXT="statemachine">
<node COLOR="#0033ff" CREATED="1481110886408" ID="ID_1019910881" LINK="https://github.com/mobxjs" MODIFIED="1481110890390" TEXT="mobX"/>
</node>
</node>
<node CREATED="1481019674445" FOLDED="true" ID="ID_1473349033" MODIFIED="1481557170129" POSITION="right" TEXT="superscript">
<node CREATED="1439382464040" ID="ID_1760591184" MODIFIED="1439382465748" TEXT="coffee"/>
<node CREATED="1481019678494" ID="ID_1879817924" MODIFIED="1481019680316" TEXT="typescript"/>
</node>
<node CREATED="1328630279282" FOLDED="true" ID="ID_1710108581" MODIFIED="1481038072015" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      html <font color="#cc0033">5</font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1357727944385" FOLDED="true" ID="ID_1246060471" MODIFIED="1480681046268" TEXT="storage">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1357718571467" FOLDED="true" ID="ID_68183139" MODIFIED="1443689832105">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0033ff">WebSQL </font>(local DB)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#009999" CREATED="1357718576288" ID="ID_217318251" LINK="http://html5doctor.com/introducing-web-sql-databases/" MODIFIED="1443689831435" TEXT="html5doctor">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357726131478" ID="ID_919279902" MODIFIED="1443689821701" TEXT="IndexedDB">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1357718994681" FOLDED="true" ID="ID_1950848694" MODIFIED="1443689829380" TEXT="LocalStorage">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#009999" CREATED="1357725693110" ID="ID_1937898562" LINK="http://www.drdobbs.com/web-development/the-localstorage-api/240000682" MODIFIED="1443689828600" TEXT="drDoobs">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#009999" CREATED="1357726006059" ID="ID_1114636876" LINK="http://graphicpush.com/using-localstorage-api-as-a-light-database" MODIFIED="1443689828596" TEXT="use as light db">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357726152430" ID="ID_573239545" LINK="http://www.html5rocks.com/en/tutorials/offline/storage/" MODIFIED="1480681042195">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>html5 rocks</b>&#160;compare
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1333449993758" FOLDED="true" ID="ID_759889651" MODIFIED="1480681028538" TEXT="performance">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1333450005828" ID="ID_915285588" LINK="http://peacekeeper.futuremark.com/" MODIFIED="1443689837415" TEXT="peacekeeper"/>
<node COLOR="#0033ff" CREATED="1333450026662" ID="ID_982974743" LINK="http://www.kevs3d.co.uk/dev/asteroidsbench/" MODIFIED="1443689837414" TEXT="asteroids"/>
<node COLOR="#0033ff" CREATED="1333450051988" ID="ID_1130689892" LINK="http://html5-benchmark.com/" MODIFIED="1443689837409" TEXT="impact"/>
</node>
<node CREATED="1357726975632" FOLDED="true" ID="ID_1614491121" MODIFIED="1480679380782" TEXT="detect">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1357726978895" ID="ID_866899770" LINK="http://modernizr.com/" MODIFIED="1480679370075" TEXT="modernizr">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1420839844439" FOLDED="true" ID="ID_84133323" MODIFIED="1480679381215" TEXT="ie fixes">
<node COLOR="#0033ff" CREATED="1420839852909" ID="ID_931257956" LINK="https://github.com/aFarkas/html5shiv" MODIFIED="1420839891169" TEXT="html5shiv"/>
<node COLOR="#0033ff" CREATED="1420839858187" ID="ID_1288575417" LINK="https://github.com/scottjehl/Respond" MODIFIED="1420839872385" TEXT="respond.js"/>
</node>
</node>
<node CREATED="1418754126986" FOLDED="true" ID="ID_1969912874" MODIFIED="1481557166664" POSITION="left" TEXT="core">
<node CREATED="1288878841397" FOLDED="true" ID="ID_1732712561" MODIFIED="1481554196622" TEXT="types">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1288878845078" FOLDED="true" ID="ID_1393940341" MODIFIED="1481554188972" TEXT="Array">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1290675371398" ID="ID_1109876137" MODIFIED="1355233971713" TEXT="remove">
<node CREATED="1288878848090" FOLDED="true" ID="ID_1345719493" MODIFIED="1355336611884" TEXT="remove(integer indices)">
<node COLOR="#0033ff" CREATED="1288878887152" ID="ID_66984295" MODIFIED="1307120301458" TEXT="array.splice(i,i)"/>
</node>
<node CREATED="1288878876243" ID="ID_34669562" MODIFIED="1355336981011" TEXT="remove(other indices)">
<node COLOR="#0033ff" CREATED="1288878903184" ID="ID_1708611684" MODIFIED="1307120302930" TEXT="delete array[&apos;text&apos;]"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1355234865664" FOLDED="true" ID="ID_460731663" MODIFIED="1436179692646" TEXT="push">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1355234871921" ID="ID_225860144" MODIFIED="1355235081126">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      var fruits = ["Banana", "Orange", "Apple", "Mango"];
    </p>
    <p>
      fruits.<b>push</b>("Kiwi")
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1310476115190" FOLDED="true" ID="ID_1977131392" MODIFIED="1355336606137" TEXT="get Unique values">
<node COLOR="#0033ff" CREATED="1310476120894" ID="ID_1433835445" MODIFIED="1310476135700">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      getUniqueValues = function (arr) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;var hash = new Object();
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;for (j = 0; j &lt; arr.length; j++) {hash[arr[j]] = true}
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;var array = new Array();
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;for (value in hash) {array.push(value)};
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;return array;
    </p>
    <p>
      }
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1342533161076" ID="ID_492563830" LINK="http://www.hunlock.com/blogs/Mastering_Javascript_Arrays" MODIFIED="1342533168059" TEXT="link"/>
<node COLOR="#0033ff" CREATED="1355235597822" ID="ID_380421879" MODIFIED="1436179695782" TEXT="shift">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336579546" FOLDED="true" ID="ID_695102395" MODIFIED="1436179695781" TEXT="slice">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1290675444181" ID="ID_250813042" MODIFIED="1355337100215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// copying (cloning) array </font>
    </p>
    <p>
      <font color="#000000">// by copying that method </font>
    </p>
    <p>
      <font color="#000000">// only simple data - String. </font>
    </p>
    <p>
      <font color="#000000">// Number and Boolean</font>
    </p>
    <p>
      var foo = [1, 2, 3];
    </p>
    <p>
      var bar = foo.slice(0);
    </p>
    <p>
      bar[1] = 5;
    </p>
    <p>
      alert(foo[1]);
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1355336573355" ID="ID_1504431818" MODIFIED="1436179695781" TEXT="sort">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336567306" ID="ID_540479072" MODIFIED="1436179695781" TEXT="splice">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336560893" ID="ID_372854107" MODIFIED="1436179695781" TEXT="toString">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336551867" ID="ID_125095957" MODIFIED="1436179695781" TEXT="unshift">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336543297" ID="ID_1737747229" MODIFIED="1436179695781" TEXT="reverse">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336535980" ID="ID_1132853151" MODIFIED="1436179695781" TEXT="pop">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336525982" ID="ID_866931670" MODIFIED="1436179695781" TEXT="lastIndexOf">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336518389" ID="ID_1873938462" MODIFIED="1436179695781" TEXT="join">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336508896" ID="ID_941979560" MODIFIED="1436179695781" TEXT="indexOf">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336498219" ID="ID_326682825" MODIFIED="1436179695781" TEXT="concat">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1355336490529" ID="ID_929396360" MODIFIED="1436179695780" TEXT="valueOf">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1342533006438" FOLDED="true" ID="ID_501702371" MODIFIED="1481554196128">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0033ff">Object </font><font color="#000000">(</font>Hash)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1342533010662" ID="ID_1480116367" LINK="http://www.mojavelinux.com/articles/javascript_hashes.html" MODIFIED="1480680067744" TEXT="javascript hashes"/>
<node COLOR="#0033ff" CREATED="1290675345898" ID="ID_1525870998" MODIFIED="1355337194543">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // copy of object&#xa0;refference
    </p>
    <p>
      var obj1 = obj
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#0033ff" CREATED="1290675327042" ID="ID_1189851193" MODIFIED="1355337194549">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // clone sample
    </p>
    <p>
      Object.prototype.<b>clone</b>&#xa0;= function() {
    </p>
    <p>
      &#xa0;&#xa0;var newObj = (this instanceof Array) ? [] : {};
    </p>
    <p>
      &#xa0;&#xa0;for (i in this) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (i == 'clone') continue;
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (this[i] &amp;&amp; typeof this[i] == "object") {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;newObj[i] = this[i].clone();
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;} else newObj[i] = this[i]
    </p>
    <p>
      &#xa0;&#xa0;} return newObj;
    </p>
    <p>
      };
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#0033ff" CREATED="1355153901837" ID="ID_1620472268" MODIFIED="1355337194554">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // removing attribute
    </p>
    <p>
      <b>delete</b>&#xa0;object.attribute.subattribute
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1355234113280" ID="ID_192641165" MODIFIED="1355337194569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      has no <b>order</b>&#xa0;with its parameters
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1290774098972" FOLDED="true" ID="ID_1567708926" MODIFIED="1481554192527" TEXT="Number">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1290774104835" ID="ID_439384427" MODIFIED="1436179673850" TEXT="parseInt">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1290774107815" ID="ID_140560400" MODIFIED="1436179673847">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      parseFloat
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1307120203127" ID="ID_1818264388" MODIFIED="1357726643142">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// alternative check fot number value</font>
    </p>
    <p>
      function isNumber(n) {
    </p>
    <p>
      &#xa0;&#xa0;return !isNaN(parseFloat(n)) &amp;&amp; isFinite(n);
    </p>
    <p>
      }
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1355325074109" FOLDED="true" ID="ID_11620303" MODIFIED="1481554194620" TEXT="Boolean">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1355325082535" ID="ID_1492842596" MODIFIED="1357726653671">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// switch between values</font>
    </p>
    <p>
      var shaded = true;
    </p>
    <p>
      shaded = <b>!</b>shaded;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1290779782717" FOLDED="true" ID="ID_1147256074" MODIFIED="1481554220765" TEXT="operations">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1290675749078" FOLDED="true" ID="ID_1873207825" MODIFIED="1481554215207" TEXT="clone">
<node COLOR="#0033ff" CREATED="1290675760303" ID="ID_1250613880" MODIFIED="1307435002764">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      function clone(obj) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;// Handle the 3 simple types, and null or undefined
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (null == obj || "object" != typeof obj) return obj;
    </p>
    <p>
      
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;// Handle Date
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (obj instanceof Date) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;var copy = new Date();
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;copy.setTime(obj.getTime());
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;return copy;
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;}
    </p>
    <p>
      
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;// Handle Array
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (obj instanceof Array) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;var copy = [];
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;for (var i = 0, var len = obj.length; i &lt; len; ++i) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;copy[i] = clone(obj[i]);
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;}
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;return copy;
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;}
    </p>
    <p>
      
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;// Handle Object
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;if (obj instanceof Object) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;var copy = {};
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;for (var attr in obj) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;}
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;return copy;
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;}
    </p>
    <p>
      
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;throw new Error("Unable to copy obj! Its type isn't supported.");
    </p>
    <p>
      }
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1295345675290" FOLDED="true" ID="ID_593110569" MODIFIED="1481554216288" TEXT="flow">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1295345687611" FOLDED="true" ID="ID_1514880238" MODIFIED="1480677043930" TEXT="switch">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1295345693386" ID="ID_996238654" MODIFIED="1357726693023">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>switch</b>(n) {<br/>case 1:<br/><i>&#xa0;&#xa0;execute code block 1</i><br/>&#xa0;&#xa0;break;<br/>case 2:<br/><i>&#xa0;&#xa0;execute code block 2</i><br/>&#xa0;&#xa0;break;<br/>default:<br/><i>&#xa0; code to be executed if n is different from case 1 and 2</i><br/>} <font color="#000000">// end switch</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1307120335181" ID="ID_518244207" MODIFIED="1480677039091" TEXT="if">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1295345684330" FOLDED="true" ID="ID_1543868029" MODIFIED="1481554217570" TEXT="loops">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1307120341316" ID="ID_1935966644" MODIFIED="1480677034255" TEXT="do">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1307120343340" ID="ID_1535278259" MODIFIED="1480677034255" TEXT="while">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1307120347644" ID="ID_13375185" MODIFIED="1480677034254" TEXT="forin">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1307120350811" ID="ID_1139759020" MODIFIED="1480677034253" TEXT="for">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1354543290974" FOLDED="true" ID="ID_1353892888" MODIFIED="1481554218537" TEXT="function">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354543297859" ID="ID_87405008" LINK="http://stackoverflow.com/questions/518000/is-javascript-a-pass-by-reference-or-pass-by-value-language" MODIFIED="1354543328950">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>SO</b>&#xa0;arguments pass by
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1290779795304" FOLDED="true" ID="ID_1529456645" MODIFIED="1481554219662" TEXT="Type Conversion">
<node CREATED="1290779800255" ID="ID_1854719369" LINK="http://www.jibbering.com/faq/notes/type-conversion/" MODIFIED="1328630117086" TEXT="links1"/>
</node>
</node>
<node CREATED="1307434903922" FOLDED="true" ID="ID_410338370" MODIFIED="1481554750307" TEXT="objects">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1357726603924" ID="ID_1228545119" MODIFIED="1436179645569" TEXT="document">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1290675458896" FOLDED="true" ID="ID_619064926" MODIFIED="1480682002629" TEXT="RegExp">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1480681974030" FOLDED="true" ID="ID_48455036" MODIFIED="1480681977901" TEXT="validations">
<node COLOR="#0033ff" CREATED="1346928182077" ID="ID_1210306501" MODIFIED="1357726459116">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// validate email</font>
    </p>
    <p>
      function validateEmail(email) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;var re = /^(([^&lt;&gt;()[\]\\.,;:\s@\"]+(\.[^&lt;&gt;()[\]\\.,;:\s@\"]+)*)|(\
    </p>
    <p>
      ".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA
    </p>
    <p>
      -Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;&#xa0;return re.test(email);
    </p>
    <p>
      }
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1307434937756" FOLDED="true" ID="ID_1495479919" MODIFIED="1480682016804" TEXT="window">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1480681991648" FOLDED="true" ID="ID_226645831" MODIFIED="1480682014959" TEXT="frames">
<node CREATED="1357825620062" ID="ID_1218066818" MODIFIED="1357825674307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      If a document contain <b>frames</b>&#xa0;(<b><font color="#0033ff">&lt;frame&gt;</font></b>&#xa0;or <b><font color="#0033ff">&lt;iframe&gt;</font></b>&#xa0; tags), the browser creates one window object for the HTML document, and one additional <b><font color="#0033ff">window</font></b>&#xa0;object for each <b>frame.</b>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1357827066710" ID="ID_1072123797" MODIFIED="1358185423743">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // frame loaded
    </p>
    <p>
      // <b>w3</b>&#xa0;browsers
    </p>
    <p>
      <font color="#0033ff">iframe.<b>onload</b>&#xa0;= refresh </font>
    </p>
    <p>
      // <b>ie</b>
    </p>
    <p>
      <font color="#0033ff">iframe.<b>addEventListener</b>('load', refresh, false);&#xa0;</font>
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#0033ff" CREATED="1358185346809" ID="ID_1915956566" MODIFIED="1480680006566">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// object that points </font>
    </p>
    <p>
      <font color="#000000">// to parent window</font>
    </p>
    <p>
      <b>window.opener</b>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1480682007871" FOLDED="true" ID="ID_551792975" MODIFIED="1480682012101" TEXT="popup">
<node CREATED="1356546558872" ID="ID_1573997681" LINK="http://stackoverflow.com/questions/3131597/closing-child-window-automatically-when-the-parent-window-is-closed" MODIFIED="1480682004543">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#009999"><b>SO:</b></font><b>&#xa0;</b>close popup win
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357726600076" FOLDED="true" ID="ID_452456734" MODIFIED="1480681989398" TEXT="location">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1358185696068" ID="ID_1027265467" LINK="http://www.w3schools.com/jsref/obj_location.asp" MODIFIED="1480680003280" TEXT="w3schools">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1319713375018" FOLDED="true" ID="ID_384948737" MODIFIED="1480681965233" TEXT="DOM">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1319713408997" FOLDED="true" ID="ID_855253354" MODIFIED="1480681964378" TEXT="option">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1319713411397" ID="ID_1032348269" MODIFIED="1357726495529">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// create elements</font>
    </p>
    <p>
      var breakfast = document.createElement("optgroup");
    </p>
    <p>
      breakfast.label = "Breakfast";
    </p>
    <p>
      
    </p>
    <p>
      var lunch = document.createElement("optgroup");
    </p>
    <p>
      lunch.label = "Lunch";
    </p>
    <p>
      
    </p>
    <p>
      var dinner = document.createElement("optgroup");
    </p>
    <p>
      dinner.label = "Dinner";
    </p>
    <p>
      
    </p>
    <p>
      var cereal = document.createElement("option");
    </p>
    <p>
      cereal.value = "cereal";
    </p>
    <p>
      cereal.appendChild(document.createTextNode("Cereal"));
    </p>
    <p>
      breakfast.appendChild(cereal);
    </p>
    <p>
      
    </p>
    <p>
      var eggs = document.createElement("option");
    </p>
    <p>
      eggs.value = "eggs";
    </p>
    <p>
      eggs.appendChild(document.createTextNode("Eggs"));
    </p>
    <p>
      breakfast.appendChild(eggs);
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#0033ff" CREATED="1319713484419" ID="ID_1711159791" MODIFIED="1357726529415">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// remove all elements</font>
    </p>
    <p>
      while (selectMenu.hasChildNodes()) {
    </p>
    <p>
      &#xa0;&#xa0;&#xa0;selectMenu.removeChild(selectMenu.firstChild);
    </p>
    <p>
      }
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1354703965541" FOLDED="true" ID="ID_1690145052" MODIFIED="1480681961093">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>ie</b>&#xa0;and <b>checked</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1354703955604" ID="ID_440168666" MODIFIED="1354703963006">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Internet Explorer doesn't like to let you change the checked value of an input that is not a part of the DOM. Try setting the checked value AFTER the item has been appended and see if that works.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357727384165" FOLDED="true" ID="ID_76096752" MODIFIED="1480681947727" TEXT="form">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1355135816899" ID="ID_293500023" MODIFIED="1357727394515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// creating js array on html page.</font>
    </p>
    <p>
      $postvalue=array("a","b","c");&#xa0;<br/>foreach($postvalue as $value)&#xa0;{&#xa0;&#xa0;<br/>&#xa0;&#xa0;echo '&lt;input type="hidden" name="result[]" value="'. $value. '"&gt;';&#xa0;<br/>}
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1438608656068" FOLDED="true" ID="ID_750477043" MODIFIED="1480681945882" TEXT="image">
<node CREATED="1438608666702" ID="ID_1574375735" MODIFIED="1438608705384">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      # get real image width and height
    </p>
    <p>
      <font color="#0000ff">img.naturalWidth </font>
    </p>
    <p>
      <font color="#0000ff">img.naturalHeight</font>
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#0033ff" CREATED="1438608850255" ID="ID_149727087" LINK="http://www.sitepoint.com/html5-responsive-design-image-dimensions/" MODIFIED="1480681938840" TEXT="responsive image attr"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1299147148648" FOLDED="true" ID="ID_1061921677" LINK="JSON.mm" MODIFIED="1480681895953" TEXT="JSON">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1355155713563" ID="ID_285164846" MODIFIED="1357726875493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">// modify for</font>&#xa0;<font color="#009999"><b>SO</b></font>
    </p>
    <p>
      var obj = JSON.parse(info);&#xa0;&#xa0;<font color="#000000">// parse the JSON into a JavaScript object&#xa0;&#xa0;</font>
    </p>
    <p>
      obj.application.proId = id; <font color="#000000">// modify the object&#xa0;&#xa0;</font>
    </p>
    <p>
      info = JSON.stringify(obj);&#xa0;&#xa0;<font color="#000000">// stringify it into JSON if you wanted it as JSON</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481554223172" ID="ID_1962067225" MODIFIED="1481554226411" TEXT="Object"/>
</node>
<node CREATED="1357837247228" FOLDED="true" ID="ID_565291198" MODIFIED="1481554742950" TEXT="functions">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1357837240154" FOLDED="true" ID="ID_587763437" MODIFIED="1481554239056" TEXT="timer">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1357837225206" FOLDED="true" ID="ID_362924911" MODIFIED="1436179653481">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      setTimeout( )
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1357837306844" ID="ID_375626964" MODIFIED="1357837336204">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // make on single call of function
    </p>
    <p>
      <b><font color="#0033ff">setTimeout</font></b><font color="#0033ff">(func, delay);</font>
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#009999" CREATED="1357837405627" ID="ID_246775735" LINK="http://javascript.ru/setTimeout" MODIFIED="1357837430139" TEXT="js.ru">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357837270941" ID="ID_222235691" MODIFIED="1436179653481" TEXT="clearTimeout( )">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#0033ff" CREATED="1357837251533" FOLDED="true" ID="ID_1732466113" MODIFIED="1436179653481" TEXT="setIneteval( )">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1357837441560" ID="ID_1038214206" MODIFIED="1357837539438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // func. call with interval<br/><b><font color="#0033ff">setInterval</font></b><font color="#0033ff">(func, delay);</font>
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#009999" CREATED="1357837395545" ID="ID_1532165150" LINK="http://javascript.ru/setInterval" MODIFIED="1357837402669" TEXT="js.ru">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1357837261640" ID="ID_1512787065" MODIFIED="1436179653480" TEXT="clearInterval( )">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1398882622279" FOLDED="true" ID="ID_1424880355" MODIFIED="1481554741443" TEXT="elements">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1398882628569" ID="ID_1398293829" LINK="http://javascript.info/tutorial/searching-elements-dom" MODIFIED="1480680030304">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#009999"><b>js info </b></font>search
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1481206149787" FOLDED="true" ID="ID_392614598" MODIFIED="1481557165336" POSITION="left" TEXT="ES2015">
<node COLOR="#0033ff" CREATED="1481277879410" FOLDED="true" ID="ID_296944219" MODIFIED="1481555362179" TEXT="const">
<node CREATED="1481555304195" ID="ID_1643910660" MODIFIED="1481555337049">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// magic numbers</i></font>
<font color="#000080"><b>function</b></font> loadProfiles(userNames) {
  <font color="#000080"><b>if</b></font> (userNames.length &gt; <font color="#0000FF">3</font>) {
    <font color="#008800"><i>//                   ^ this is magic number</i></font>
  } <font color="#000080"><b>else</b></font> {
    <font color="#008800"><i>// ...</i></font>
  }

  <font color="#000080"><b>if</b></font> (someValue &gt; <font color="#0000FF">3</font> ) {}
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555341118" ID="ID_178445544" MODIFIED="1481555356804">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// fix by using const</i></font>
<font color="#008800"><i>// const allow to use read-only named constants</i></font>
<font color="#000080"><b>function</b></font> loadProfiles(userNames) {
  
  <font color="#000080"><b>const</b></font> MAX_USERS = <font color="#0000FF">3</font>;
  <font color="#008800"><i>// ^ must be assigned an initial value</i></font>
  <font color="#000080"><b>const</b></font> MAX_USERS;
  <font color="#008800"><i>// ^ will gegerate an error</i></font>
  MAX_USERS = <font color="#0000FF">10</font>;
  <font color="#008800"><i>// ^ syntax error goes here</i></font>

  <font color="#000080"><b>if</b></font> (userName.length &gt; MAX_USERS) {
    <font color="#000080"><b>const</b></font> MAX_REPLIES = <font color="#0000FF">15</font>;
    <font color="#008800"><i>// ^ const is block scoped so </i></font>
    <font color="#008800"><i>//   Not available outside if block</i></font>
  } <font color="#000080"><b>else</b></font> {
    <font color="#008800"><i>// ...</i></font>
  }

  <font color="#000080"><b>const</b></font> MAX_REPLIES = <font color="#0000FF">3</font>;
  <font color="#000080"><b>if</b></font> (someElement &gt; MAX_REPLIES) {}
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481278052844" FOLDED="true" ID="ID_647131404" MODIFIED="1481557163154" TEXT="functions">
<node CREATED="1481278709454" FOLDED="true" ID="ID_709506743" MODIFIED="1481556901749" TEXT="default_params">
<node CREATED="1481278738075" ID="ID_336604028" MODIFIED="1481278795256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Functions</i></font>
loadProfiles([<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>]);
<font color="#008800"><i>// no arguments or undefined</i></font>
loadProfiles();
loadProfiles(<font color="#000080"><b>undefined</b></font>);
<font color="#008800"><i>// will get TypeUndefied error Cannot read property 'legth' of undefined</i></font>
<font color="#008800"><i>// when we call fuction expecting arguments the argument will became undefined</i></font>
<font color="#008800"><i>// fix by usign oldsytle</i></font>
<font color="#000080"><b>function</b></font> loadProfiles(userNames) {
  <font color="#000080"><b>let</b></font> names = <font color="#000080"><b>typeof</b></font> userNames !== <font color="#0000FF">'undefined'</font> ? userNames : [];
  <font color="#000080"><b>let</b></font> namesLength = names.length
  <font color="#008800"><i>// ...</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481278796823" ID="ID_1693302844" MODIFIED="1481278839287">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// fix new - default parameters for functions</i></font>
<font color="#000080"><b>function</b></font> loadProfiles(userNames = []) {
  <font color="#000080"><b>let</b></font> namesLength = userNames.length;
  console.log(namesLength);
}
<font color="#008800"><i>// run </i></font>
<font color="#008800"><i>// loadProfiles(); doesn't brake</i></font>
<font color="#008800"><i>// loadProfiles(undefined); doesn't brake</i></font></pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481278842185" FOLDED="true" ID="ID_470400844" MODIFIED="1481556900196" TEXT="options parameter">
<node CREATED="1481278847172" ID="ID_965228811" MODIFIED="1481278983716">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Options Object</i></font>
<font color="#008800"><i>// option object is the second argument here</i></font>
<font color="#008800"><i>// caveats:</i></font>
<font color="#008800"><i>//  - it' unclear to for caller what parameters </i></font>
<font color="#008800"><i>//    is supported</i></font>
<font color="#000080"><b>function</b></font> setPageThread(name, options = {}) {
  <font color="#000080"><b>let</b></font> popular = options.popular;
  <font color="#000080"><b>let</b></font> expires = options.expires;
  <font color="#000080"><b>let</b></font> activeClass = options.activeClass;
  <font color="#008800"><i>// ...</i></font>
}

<font color="#008800"><i>// call function</i></font>
setPageThread(<font color="#0000FF">&quot;new Version out Soon!&quot;</font>, {
  popular: <font color="#000080"><b>true</b></font>,
  expires: <font color="#0000FF">10000</font>,
  activeClass: <font color="#0000FF">&quot;is-page_thread&quot;</font>
});</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481278954642" ID="ID_362277460" MODIFIED="1481279136584">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// <b>named parameters</b></i></font>
fucntion setPageThread(name, {  popular, expires, activeClass }) {
  consloe.log (<font color="#0000FF">&quot;Name&quot;</font>, name);
  consloe.log (<font color="#0000FF">&quot;Popular&quot;</font>, popular);
  consloe.log (<font color="#0000FF">&quot;Expires&quot;</font>, expires);
  consloe.log (<font color="#0000FF">&quot;Active&quot;</font>, activeClass);
}
<font color="#008800"><i>// calling that new func</i></font>
setPageThread(<font color="#0000FF">&quot;New Version out Soon!&quot;</font>, {
  popular: <font color="#000080"><b>true</b></font>, 
  expires: <font color="#0000FF">10000</font>,
  activeClass: <font color="#0000FF">&quot;is-page-thread&quot;</font>
});</pre>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1481278986075" ID="ID_1288323414" MODIFIED="1481279129112">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// when we <b>omit some options</b></i></font>
setPageThread(<font color="#0000FF">&quot;New Version out Soon!&quot;</font>, {
  popular: <font color="#000080"><b>true</b></font> 
});
<font color="#008800"><i>// we got </i></font>
<font color="#008800"><i>// Name: New Version out Soon!</i></font>
<font color="#008800"><i>// Popular: true</i></font>
<font color="#008800"><i>// Expires: undefined</i></font>
<font color="#008800"><i>// Active: undefined</i></font>
</pre>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1481279095901" ID="ID_757773702" MODIFIED="1481279118879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// another caveat when <b>without</b> optioins object</i></font>
setPageThread(<font color="#0000FF">&quot;New Version out Soon!&quot;</font>)
<font color="#008800"><i>// that call will break the execution with options set to undefined</i></font>
<font color="#008800"><i>// user default parameter</i></font>
<font color="#000080"><b>function</b></font> setPageThread(name, { popular, expires, activeClass } = {}) {
  <font color="#008800"><i>// ....</i></font>
}
setPageThread(<font color="#0000FF">&quot;New Version out Soon!&quot;</font>)
<font color="#008800"><i>// doesn't raise an error</i></font></pre>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1481279179468" FOLDED="true" ID="ID_901760331" MODIFIED="1481556898621" TEXT="rest parameter syntax">
<node CREATED="1481279186645" ID="ID_1900548227" MODIFIED="1481279245373">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i><b>// Variatic functions</b></i></font>
<font color="#008800"><i>// arguments object</i></font>
<font color="#008800"><i>// Caveats function signature is not clear</i></font>
<font color="#000080"><b>function</b></font> displayTags() {
  <font color="#008800"><i>// caveat where this variable came from</i></font>
  <font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> i <font color="#000080"><b>in</b></font> arguments) {
    <font color="#000080"><b>let</b></font> tag = arguments[i];
    _addToTopic(tag);
  }
}
<font color="#008800"><i>// also if we use explicit another argument it will break functionality</i></font>
<font color="#000080"><b>function</b></font> displayTags(targetElement) {
  <font color="#000080"><b>let</b></font> target = _findElement <font color="#008800"><i>// here arguments[0] won't be the first tag element</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481279247802" ID="ID_1925014828" MODIFIED="1481279324445">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// use <b>rest parameter syntax</b></i></font>
<font color="#008800"><i>// 3 dots are part of the syntax</i></font>
<font color="#008800"><i>// tags is array of tags arguments</i></font>
<font color="#008800"><i>// last parameter should be last parameter in a function</i></font>
<font color="#000080"><b>function</b></font> displayTags(<b>...tags</b>) {
  <font color="#000080"><b>for</b></font> (<font color="#000080"><b>let</b></font> i <font color="#000080"><b>in</b></font> tags) {
    <font color="#000080"><b>let</b></font> tag = 
  }
}
<font color="#008800"><i>// 2nd variant </i></font>
<font color="#000080"><b>function</b></font> displayTags(targetElement, <b>...tags</b>) {
  <font color="#000080"><b>let</b></font> target = _findElement(targetElement);

  <font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> i <font color="#000080"><b>in</b></font> tags) {
    <font color="#000080"><b>let</b></font> tag = tags[i];
    _addToTopic(target, tag);
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481279343001" FOLDED="true" ID="ID_1139432465" MODIFIED="1481554253770" TEXT="spread operator">
<node CREATED="1481279348379" ID="ID_406435403" MODIFIED="1481279375000">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Split array into arguments</i></font>
<font color="#008800"><i>// data.tags is and array of arguments</i></font>
<font color="#008800"><i>// [&quot;programing&quot;, &quot;web&quot;, &quot;HTML&quot;]</i></font>
getRequest(<font color="#0000FF">&quot;/topics/17/tags&quot;</font>, <font color="#000080"><b>function</b></font> (data) {
  <font color="#000080"><b>let</b></font> tags = data.tags;
  <font color="#008800"><i>// display tags doesnt' accept array argument but many arguments</i></font>
  displayTags(tags);
})</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481279377832" ID="ID_1676792822" MODIFIED="1481279417074">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// use spread operator</i></font>
getRequest(<font color="#0000FF">&quot;/topics/17/tags&quot;</font>, <font color="#000080"><b>function</b></font> (data) {
  <font color="#000080"><b>let</b></font> tags = data.tags;
  <font color="#008800"><i>// three dots but used in function invocation</i></font>
  displayTags(<b>...tags</b>);
  <font color="#008800"><i>// displayTags(tag, tag, tag)</i></font>
})</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481552010337" FOLDED="true" ID="ID_1966493036" MODIFIED="1481554257279" TEXT="options param">
<node CREATED="1481552022893" ID="ID_1832967127" MODIFIED="1481552058223">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// functions with custom options</i></font>
<font color="#008800"><i>// with functions that need to get many parameters</i></font>
<font color="#008800"><i>// it's ok to use single options parameter across the </i></font>
<font color="#008800"><i>// application</i></font>
<font color="#008800"><i>// old sytle with default values</i></font>
<font color="#000080"><b>function</b></font> countdownTimer(target, timeLeft, options = {}) {
  <font color="#000080"><b>let</b></font> container = options.container || <font color="#0000FF">&quot;.timer-display&quot;</font>;
  <font color="#000080"><b>let</b></font> timeUnit = options.timeUnit || <font color="#0000FF">&quot;seconds&quot;</font>;
  <font color="#000080"><b>let</b></font> clonedDataAttribte = options.clonedDataAttribte || <font color="#0000FF">&quot;cloned&quot;</font>;
  <font color="#000080"><b>let</b></font> timeoutClass = options.timeoutClass || <font color="#0000FF">&quot;.is-timeout&quot;</font>;
  <font color="#000080"><b>let</b></font> timeoutSoonClass = options.timeoutSoonClass || <font color="#0000FF">&quot;.is-timeout-soon&quot;</font>;
  <font color="#000080"><b>let</b></font> timeouteSoonTime = options.timeouteSoonTime || <font color="#0000FF">10</font>;
  <font color="#008800"><i>// ...</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481552064093" ID="ID_1714876978" MODIFIED="1481552173578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// use local object to group all default values</i></font>
<font color="#000080"><b>function</b></font> countdownTimer(target, timeLeft, options = {}) {
  
  <font color="#000080"><b>let</b></font> <font color="#000080"><b>default</b></font> = {
    container: <font color="#0000FF">&quot;.timer-display&quot;</font>,
    timeUnit: <font color="#0000FF">&quot;seconds&quot;</font>, 
    clonedDataAttribte: <font color="#0000FF">&quot;cloned&quot;</font>,
    timeoutClass: <font color="#0000FF">&quot;.is-timeout&quot;</font>,
    timeoutSoonClass: <font color="#0000FF">&quot;.is-timeout-soon&quot;</font>,
    timeouteSoonTime: <font color="#0000FF">10</font>
  };

  <font color="#008800"><i>// ...</i></font>
}

<font color="#008800"><i>// Merging options</i></font>
<font color="#008800"><i>// Merging options must override default options object</i></font>
<font color="#008800"><i>// Object.assign method - copies properties from one object to target object</i></font>
<font color="#000080"><b>let</b></font> settings = <b>Object.assign</b>({}, defaults, options)
<font color="#008800"><i>// here {} - target object (which is mutated and returned)</i></font>
<font color="#008800"><i>//      defaults - default values object</i></font>
<font color="#008800"><i>//      options - new options for settings</i></font>

<font color="#008800"><i>// Many options params </i></font>
<font color="#000080"><b>let</b></font> settings = <b>Objects.assign</b>({}, defaults, options, options1, options2)
<font color="#008800"><i>// in case above options2 override options1 &amp; options1 overrride options</i></font>

<font color="#008800"><i>// <b>case to preserve default value</b> </i></font>
<font color="#008800"><i>// when time unit changes there are need to change time values</i></font>
<font color="#000080"><b>function</b></font> countdownTimer(target, timeLeft, options ={}) {
  <font color="#000080"><b>let</b></font> defaults = {
    <font color="#008800"><i>// ...</i></font>
  };

  <font color="#000080"><b>let</b></font> settings = Object.assign({}, defaults, options);
  <font color="#000080"><b>if</b></font> (settins.timeUnit !== defaults.timeUnit) {
    _conversionFinctions(timeLeft, settings.timeUnit);
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481556879608" FOLDED="true" ID="ID_386309454" MODIFIED="1481557159486" TEXT="generator">
<node CREATED="1481556893108" ID="ID_427480375" MODIFIED="1481556944388">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// generator fucntion</i></font>
<font color="#008800"><i>// the function * declaration defines generator fun-s.</i></font>
<font color="#008800"><i>// These functons from which we can return 'yield' iterator</i></font>
<font color="#008800"><i>// objects.</i></font>
<font color="#000080"><b>function</b></font> *nameList() {
  yield <font color="#0000FF">&quot;Sam&quot;</font>;
  <font color="#008800"><i>// ^ atomaticly assign to  { done: false, value: &quot;Sam&quot; }</i></font>
  yield <font color="#0000FF">&quot;Tyler&quot;</font>;
  <font color="#008800"><i>//  ^ the same { done: false, value: &quot;Tyler&quot; }</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556946658" ID="ID_1421208577" MODIFIED="1481556969724">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// declaration syntax</i></font>
<font color="#000080"><b>function</b></font> *nameList(argument) {
<font color="#000080"><b>function</b></font> * nameList(argument) {
<font color="#000080"><b>function</b></font>* nameList(argument) {</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556970846" ID="ID_1956287508" MODIFIED="1481557006847">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// generator function provide object for iterator,</i></font>
<font color="#008800"><i>// deconstructing and spread operator</i></font>
<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> name of nameList()) {
  <font color="#008800"><i>//            ^ return generator object</i></font>
  console.log( name );
}
<font color="#008800"><i>// &gt; Sam</i></font>
<font color="#008800"><i>// &gt; Tyler</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481557053556" ID="ID_1933318687" MODIFIED="1481557056662">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// spread operator</i></font>
<font color="#000080"><b>let</b></font> names = [...nameList()];
console.log( names );
<font color="#008800"><i>// &gt; [&quot;Sam&quot;, &quot;Tyler&quot;]</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481557058303" ID="ID_924834175" MODIFIED="1481557083977">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// destructuring</i></font>
<font color="#000080"><b>let</b></font> [first, second] = nameList();
console.log( first, second );
<font color="#008800"><i>// &gt; Sam Tyler</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481557102966" FOLDED="true" ID="ID_41120171" MODIFIED="1481557154176" TEXT="usage">
<node CREATED="1481557104946" ID="ID_1804263873" MODIFIED="1481557114826">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Update post object</i></font>
<font color="#000080"><b>let</b></font> post = { titile: <font color="#0000FF">&quot;New Features in JS&quot;</font>, replies: <font color="#0000FF">19</font> };

post[Symbol.iterator] = <font color="#000080"><b>function</b></font> *() {
  <font color="#008800"><i>//                             ^ generator function signature</i></font>
  <font color="#000080"><b>let</b></font> properties = Object.keys(<font color="#000080"><b>this</b></font>)
  <font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> p of properties){
    yield <font color="#000080"><b>this</b></font>[p]
  <font color="#008800"><i>// ^ first 'titile' and the 'replies'</i></font>
  }
}
<font color="#008800"><i>//^ Same as manually return each property value</i></font>
post[Symbol.iterator] = <font color="#000080"><b>function</b></font> * (){
  yield <font color="#000080"><b>this</b></font>.title;
  yield <font color="#000080"><b>this</b></font>.replies;
}


<font color="#000080"><b>for</b></font> (<font color="#000080"><b>let</b></font> p of post){
  console.log( p );
}
<font color="#008800"><i>// &gt; New Features in JS</i></font>
<font color="#008800"><i>// &gt; 19</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481206154625" FOLDED="true" ID="ID_457120365" MODIFIED="1481556890525" TEXT="let">
<node CREATED="1481277561413" FOLDED="true" ID="ID_306154995" MODIFIED="1481552649891" TEXT="hoisting">
<node CREATED="1481276685920" ID="ID_1261399453" MODIFIED="1481277403958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Hoisting </i></font>
<font color="#008800"><i>// loadProfile([&quot;Sam&quot;, &quot;Tyler&quot;, &quot;Brook&quot;, &quot;Alex&quot;])</i></font>
<font color="#000080"><b>function</b></font> loadProfile(userNames){
  <font color="#008800"><i>// JavasScript runtime automatically moves </i></font>
  <font color="#000080"><b>var</b></font> flashMessage, landingMessage;
  <font color="#000080"><b>if</b></font> (userNames.length &gt; <font color="#0000FF">3</font>) {
    <font color="#008800"><i>// var hoisting here actually moved to function declaraiton</i></font>
    <font color="#000080"><b>var</b></font> landingMessage = <font color="#0000FF">&quot;This might take a while&quot;</font>;
    _displaySpinner(loadingMessage);
  } <font color="#000080"><b>else</b></font> {
    <font color="#008800"><i>// var </i></font>
    flashMessage = <font color="#0000FF">&quot;Loding Profiles&quot;</font>;
    _displayFlash(flashMessage); 
  }
}
console.log(flashMessage) <font color="#008800"><i>// should generate an error</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481276938666" ID="ID_376068924" MODIFIED="1481277452022">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// let declares </i></font>
<font color="#008800"><i>// let variables are scoped to nearest block and not function</i></font>
<font color="#008800"><i>// new reworked function with using let</i></font>
<font color="#000080"><b>function</b></font> loadProfile(userNames){
  <font color="#000080"><b>if</b></font> (userNames.length &gt; <font color="#0000FF">3</font>) {
    <font color="#008800"><i>// var hoisting here actually moved to function declaraiton</i></font>
    <font color="#000080"><b>let</b></font> landingMessage = <font color="#0000FF">&quot;This might take a while&quot;</font>;
    _displaySpinner(loadingMessage);
  } <font color="#000080"><b>else</b></font> {
    <font color="#008800"><i>// var </i></font>
    <font color="#000080"><b>let</b></font> flashMessage = <font color="#0000FF">&quot;Loding Profiles&quot;</font>;
    _displayFlash(flashMessage); 
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481277479330" FOLDED="true" ID="ID_707735530" MODIFIED="1481552653626" TEXT="for in">
<node CREATED="1481277485249" ID="ID_1017274532" MODIFIED="1481277523328">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#000080"><b>function</b></font> loadProfile(userNames){
  <font color="#008800"><i>// ... Here var hoiseted to function declaration so i </i></font>
  <font color="#008800"><i>// shared for callback function</i></font>
  <font color="#000080"><b>for</b></font>(<font color="#000080"><b>var</b></font> i <font color="#000080"><b>in</b></font> userNames){
    _fetchProfile(<font color="#0000FF">&quot;/users/&quot;</font> + userNames[i], <font color="#000080"><b>function</b></font>(){
      <font color="#008800"><i>// here we are accessing i from callback</i></font>
      console.log(<font color="#0000FF">&quot;Fetched for &quot;</font>, userNames[i]);
    })
  }
}
<font color="#008800"><i>// after run for with  loadProfile([&quot;Sam&quot;, &quot;Tyler&quot;, &quot;Brook&quot;, &quot;Alex&quot;])</i></font>
<font color="#008800"><i>// &gt; Fetched for Alex</i></font>
<font color="#008800"><i>// &gt; Fetched for Alex</i></font>
<font color="#008800"><i>// &gt; Fetched for Alex</i></font>
<font color="#008800"><i>// &gt; Fetched for Alex</i></font></pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481277524624" ID="ID_1651293533" MODIFIED="1481277551909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// fixing with let which is connected to the last block</i></font>
<font color="#000080"><b>function</b></font> loadProfile(userNames){
  <font color="#008800"><i>// let is using here no sharing between fucntions </i></font>
  <font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> i <font color="#000080"><b>in</b></font> userNames){
    _fetchProfile(<font color="#0000FF">&quot;/users/&quot;</font> + userNames[i], <font color="#000080"><b>function</b></font>(){
      <font color="#008800"><i>// here we are accessing i from callback</i></font>
      console.log(<font color="#0000FF">&quot;Fetched for &quot;</font>, userNames[i]);
    })
  }
}
<font color="#008800"><i>// &gt; Fetched for Sam</i></font>
<font color="#008800"><i>// &gt; Fetched for Tyler</i></font>
<font color="#008800"><i>// &gt; Fetched for Brook</i></font>
<font color="#008800"><i>// &gt; Fetched for Alex</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481277875608" FOLDED="true" ID="ID_826273116" MODIFIED="1481552655651" TEXT="redeclare">
<node CREATED="1481278001273" ID="ID_207928192" MODIFIED="1481278005310">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// let cant be redeclared</i></font>
<font color="#000080"><b>let</b></font> flashMessage = <font color="#0000FF">&quot;Hello&quot;</font>;
<font color="#000080"><b>let</b></font> flashMessage = <font color="#0000FF">&quot;Goodbye&quot;</font>; <font color="#008800"><i>// will got TypeError here</i></font>

<font color="#008800"><i>// declaration in another scope</i></font>
<font color="#008800"><i>// so flashMessage have different scopes</i></font>
<font color="#000080"><b>let</b></font> flashMessage = <font color="#0000FF">&quot;Hello&quot;</font>;
<font color="#000080"><b>function</b></font> loadProfiles(userNames) {
  <font color="#000080"><b>let</b></font> flashMessage = <font color="#0000FF">&quot;Loading profiles&quot;</font>;
  <font color="#000080"><b>return</b></font> flashMessage;
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
</node>
<node CREATED="1481279440383" FOLDED="true" ID="ID_577744401" MODIFIED="1481556890524" TEXT="objects">
<node CREATED="1481551810508" FOLDED="true" ID="ID_1979842603" MODIFIED="1481554549588" TEXT="functions">
<node COLOR="#0033ff" CREATED="1481279697720" FOLDED="true" ID="ID_1883230735" MODIFIED="1481554547338" TEXT="=&gt;">
<node CREATED="1481279494946" ID="ID_1628920073" MODIFIED="1481279609067">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// used to encapsulate the same functionality</i></font>
<font color="#000080"><b>function</b></font> TagComponent(target, urlPath) {
  <font color="#008800"><i>// properties set on constructor object</i></font>
  <font color="#000080"><b>this</b></font>.targetElement = target;
  <font color="#000080"><b>this</b></font>.urlPath = urlPath;
}

TagComponent.prototype.render = <font color="#000080"><b>function</b></font> () {
  <font color="#008800"><i>// properties can accessed in instance mehtods</i></font>
  getRequest(<font color="#000080"><b>this</b></font>.urlPath), <font color="#000080"><b>function</b></font> (data) {
    <font color="#008800"><i>// ...</i></font>
  }
}

<i><font color="#008800">// It has caveaut with scope in callback functions
// scope fo TagComponent is antoher than scoppe of getRequest fucntions
// this.targetElement in getRequest doesn't exist end return undefined</font></i></pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node COLOR="#338800" CREATED="1481279507591" ID="ID_938361369" MODIFIED="1481279638254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// creating object</i></font>
<font color="#000080"><b>let</b></font> tagComponent = <font color="#000080"><b>new</b></font> TagComponent(targetDiv, <font color="#0000FF">&quot;/topics/17/tags&quot;</font>)
tagComponent.render(); </pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481279644236" ID="ID_993527687" MODIFIED="1481279690338">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Arrow function aka lexical binding</i></font>
TagComponent.prototype.render = <font color="#000080"><b>function</b></font> () {
  <font color="#008800"><i>// here we use <b>arrow function</b></i></font>
  <font color="#008800"><i>// it bind the scope where it was defined not where it was executed</i></font>
  getRequest(<font color="#000080"><b>this</b></font>.urlPath, (date) =&gt; {
    <font color="#000080"><b>let</b></font> tags = data.tags;
    displayTags(<font color="#000080"><b>this</b></font>.targetElement, ...tags);
  })
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481551815500" FOLDED="true" ID="ID_428830575" MODIFIED="1481554549205" TEXT="add">
<node CREATED="1481551817165" ID="ID_936544934" MODIFIED="1481551839154">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Adding function to an object</i></font>
<font color="#008800"><i>// old way</i></font>
<font color="#000080"><b>function</b></font> buildUser(first, last, postCount) {
  <font color="#000080"><b>let</b></font> fullName = first + <font color="#0000FF">&quot; &quot;</font> + last;
  <font color="#000080"><b>const</b></font> ACTIVE_POST_COUNT = <font color="#0000FF">10</font>;

  <font color="#000080"><b>return</b></font> {
    first,
    last,
    fullName,
    isActive: <font color="#000080"><b>function</b></font> () {
      <font color="#000080"><b>return</b></font> postCount &gt;= ACTIVE_POST_COUNT;
    }
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481551841399" ID="ID_37833072" MODIFIED="1481551878103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// using new short hand notation </i></font>
<font color="#000080"><b>function</b></font> buildUser(first, last, postCount) {
  <font color="#008800"><i>// ...</i></font>
  <font color="#000080"><b>return</b></font> {
    <font color="#008800"><i>//  here is shorthand notation for function</i></font>
    isActive() {
      <font color="#000080"><b>return</b></font> postCount &gt;= ACTIVE_POST_COUNT;
    }
  }
  <font color="#008800"><i>// ...</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node CREATED="1481551423710" FOLDED="true" ID="ID_825451925" MODIFIED="1481554552670" TEXT="create">
<node COLOR="#0033ff" CREATED="1481554336947" FOLDED="true" ID="ID_627416870" MODIFIED="1481554416301" TEXT="return">
<node CREATED="1481551427514" ID="ID_525957951" MODIFIED="1481551463185">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// simplify creating objects</i></font>
<font color="#008800"><i>// old style of creating obj</i></font>
<font color="#008800"><i>// which is unusefull and repeatable in many cases</i></font>
<font color="#008800"><i>// as well as duplication </i></font>
<font color="#000080"><b>function</b></font> buildUser(first, last) {
  <font color="#000080"><b>let</b></font> fullName = first + <font color="#0000FF">&quot; &quot;</font> + last;

  <font color="#000080"><b>return</b></font> { first: first, last: last, fullName: fullName }
} 
<font color="#008800"><i>// Calling the buildUser</i></font>
<font color="#000080"><b>let</b></font> user = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);

console.log( user.first );
console.log( user.last );
console.log( user.fullName );

<font color="#008800"><i>// &gt; Sam</i></font>
<font color="#008800"><i>// &gt; Williams</i></font>
<font color="#008800"><i>// &gt; Sam Williams</i></font></pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481551467024" ID="ID_1845892165" MODIFIED="1481551602647">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// New way</i></font>
<font color="#000080"><b>function</b></font> buil(first, last) {
  <font color="#000080"><b>let</b></font> fullName = first + <font color="#0000FF">&quot; &quot;</font> + last;
  <font color="#008800"><i>// way cleaner</i></font>
  <font color="#000080"><b>return</b></font> { first, last, fullName };
}
<font color="#008800"><i>// Calling the buildUser</i></font>
<font color="#000080"><b>let</b></font> user = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);
console.log( user.first );
console.log( user.last );
console.log( user.fullName );

<font color="#008800"><i>// output the same result</i></font>
<font color="#008800"><i>// &gt; Sam</i></font>
<font color="#008800"><i>// &gt; Williams</i></font>
<font color="#008800"><i>// &gt; Sam Williams</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481554347702" FOLDED="true" ID="ID_1262338250" MODIFIED="1481554551185" TEXT="old">
<node CREATED="1481554397874" ID="ID_238529189" MODIFIED="1481554482698">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// using a function approach</i></font>
<font color="#000080"><b>function</b></font> SponsorWidget(name, description, url) {
  <font color="#000080"><b>this</b></font>.name = name;
  <font color="#000080"><b>this</b></font>.description = description;
  <font color="#000080"><b>this</b></font>.url = url;
}
<font color="#008800"><i>// ^ constructor function are invoked via <b>new</b> operator</i></font>

SponsorWidget.prototype.render = <font color="#000080"><b>function</b></font> () {
  <font color="#008800"><i>// ..</i></font>
}

<font color="#008800"><i>// create object</i></font>
<font color="#000080"><b>let</b></font> sponsorWidget = <font color="#000080"><b>new</b></font> SponsorWidget(name, description, url);
sponsorWidget.render();</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481554395175" FOLDED="true" ID="ID_1222507238" MODIFIED="1481554552197" TEXT="new">
<node CREATED="1481554400732" ID="ID_202685070" MODIFIED="1481554525156">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// new Class syntax (it's syntaxing sugar)</i></font>
<font color="#000080"><b>class</b></font> SponsorWidget {
  constructor(name, description, url) {
    <font color="#008800"><i>// ^ also runs every time new instance</i></font>
    <font color="#008800"><i>//   reated with new operator</i></font>
    <font color="#000080"><b>this</b></font>.name = name;
    <font color="#000080"><b>this</b></font>.description = name;
    <font color="#000080"><b>this</b></font>.url = url;
    <font color="#008800"><i>// ^ assigning to instance vars make</i></font>
    <font color="#008800"><i>//   variables visible to other instance</i></font>
    <font color="#008800"><i>//   methods</i></font>
  }

  <font color="#008800"><i>// just like object method definitions</i></font>
  render() {
    <font color="#000080"><b>let</b></font> link = <font color="#000080"><b>this</b></font>._buildlink(<font color="#000080"><b>this</b></font>.url);
    <font color="#008800"><i>// ...</i></font>
  }

  <font color="#008800"><i>// there is no private and protected modifiers</i></font>
  <font color="#008800"><i>// prefixiing with undescore is conventions it should</i></font>
  <font color="#008800"><i>// not be executed from publuc API</i></font>
  _buildlink(url) {
    <font color="#008800"><i>// ...</i></font>
  }
}
<font color="#008800"><i>// create Instance</i></font>
<font color="#000080"><b>let</b></font> sponsorWidget = <font color="#000080"><b>new</b></font> SponsorWidget(name, description, url);
sponsorWidget.render();</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node CREATED="1481551690326" FOLDED="true" ID="ID_1115623149" MODIFIED="1481554691988" TEXT="initializer">
<node CREATED="1481551698336" ID="ID_1565860405" MODIFIED="1481551721852">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Object initializer shorthand (from variables to object)</i></font>
<font color="#000080"><b>let</b></font> name = <font color="#0000FF">&quot;Sam&quot;</font>;
<font color="#000080"><b>let</b></font> age = <font color="#0000FF">45</font>;
<font color="#000080"><b>let</b></font> friends = [<font color="#0000FF">&quot;Brook&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>];

<font color="#000080"><b>let</b></font> user = { name, age, friends };
<font color="#008800"><i>// let user = { name: name, age: age, friends: friends }</i></font>

console.log( user.name );
console.log( user.age );
console.log( user.friends );

<font color="#008800"><i>// &gt; Sam</i></font>
<font color="#008800"><i>// &gt; 45</i></font>
<font color="#008800"><i>// &gt; [&quot;Brook&quot;, &quot;Tyler&quot;]</i></font></pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481554576789" FOLDED="true" ID="ID_1203786057" MODIFIED="1481554715793" TEXT="inheritance">
<node CREATED="1481554581784" ID="ID_470247159" MODIFIED="1481554616795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Parent Class</i></font>
<font color="#000080"><b>class</b></font> Widget {
  constructor() {
    <font color="#000080"><b>this</b></font>.baseCSS = <font color="#0000FF">&quot;site-widget&quot;</font>;
  }

  parse(value){
    <font color="#008800"><i>//...</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481554619988" ID="ID_884609338" MODIFIED="1481554637090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Child Class</i></font>
<font color="#000080"><b>class</b></font> SponsorWidget <font color="#000080"><b>extends</b></font> Widget {
  <font color="#008800"><i>// here we used the extend keyword</i></font>
  constructor(name, description, url){
    <font color="#000080"><b>super</b></font>();
    <font color="#008800"><i>// ^ the super methods runs the constructor</i></font>
    <font color="#008800"><i>//   form the parent class</i></font>
    <font color="#008800"><i>// ...</i></font>
  }

  render(){
    <font color="#000080"><b>let</b></font> parsedName = <font color="#000080"><b>this</b></font>.parse(<font color="#000080"><b>this</b></font>.name);
    <font color="#008800"><i>//                    ^ also it parses the </i></font>
    <font color="#008800"><i>//                      parent class methods</i></font>
    <font color="#000080"><b>let</b></font> css = <font color="#000080"><b>this</b></font>._buildlCSS(<font color="#000080"><b>this</b></font>.baseCSS);
    <font color="#008800"><i>// ...                         ^ and inherit properties</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481554647303" FOLDED="true" ID="ID_1267401418" MODIFIED="1481554714578" TEXT="override">
<node CREATED="1481554650318" ID="ID_192801817" MODIFIED="1481554676285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Overriding </i></font>
<font color="#000080"><b>class</b></font> SponsorWidget <font color="#000080"><b>extends</b></font> Widget {
  constructor(name, description, url){
    <font color="#000080"><b>super</b></font>();
    <font color="#008800"><i>// ...</i></font>
  }

  parse(){
    <font color="#008800"><i>// below we call parent vesrsoin of parse() method </i></font>
    <font color="#000080"><b>let</b></font> parsedName = <font color="#000080"><b>super</b></font>.parse(<font color="#000080"><b>this</b></font>.name);
    <font color="#008800"><i>//                 ^ with super keyword</i></font>
    <font color="#000080"><b>return</b></font> <font color="#a61717">`</font>Sponsor: ${parsedName}<font color="#a61717">`</font>;
  }

  render(){
    <font color="#008800"><i>// ...</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481551746242" FOLDED="true" ID="ID_540398276" MODIFIED="1481554564955" TEXT="destructuring">
<node CREATED="1481551751551" ID="ID_161043675" MODIFIED="1481551762747">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Object Destructuring (from object to variables)</i></font>
<font color="#000080"><b>let</b></font> user = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);
<font color="#000080"><b>let</b></font> first = user.first;
<font color="#000080"><b>let</b></font> last = user.last;
<font color="#000080"><b>let</b></font> fullName = user.fullName;

<font color="#008800"><i>// is the same sas</i></font>
<font color="#000080"><b>let</b></font> { first, last, fullName } = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);

<font color="#008800"><i>// print</i></font>
console.log( first );
console.log( last );
console.log( fullName );

<font color="#008800"><i>// output</i></font>
<font color="#008800"><i>// &gt; Sam</i></font>
<font color="#008800"><i>// &gt; Williams</i></font>
<font color="#008800"><i>// &gt; Sam Williams</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481551763747" ID="ID_1864136890" MODIFIED="1481551780221">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Take specific paramters form object</i></font>
<font color="#000080"><b>let</b></font> { fullName } = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);
console.log( fullName ) <font color="#008800"><i>// &gt; Sam Williams</i></font>

<font color="#000080"><b>let</b></font> { last, fullName } = buildUser(<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Williams&quot;</font>);
console.log(  last );      <font color="#008800"><i>// &gt; Williams</i></font>
console.log(  fullName );  <font color="#008800"><i>// &gt; Sam Williams</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481551904829" FOLDED="true" ID="ID_325886221" MODIFIED="1481552667418" TEXT="String">
<node CREATED="1481551908453" FOLDED="true" ID="ID_1301896079" MODIFIED="1481552667058" TEXT="literals">
<node CREATED="1481551911670" ID="ID_394332814" MODIFIED="1481551936830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// String literals</i></font>
<font color="#008800"><i>// Support string interpolation</i></font>
<font color="#008800"><i>// user backticks </i></font>
<font color="#000080"><b>function</b></font> buildUser(first, last, postCount) {
  <font color="#008800"><i>//  here we using string interpolation with $ symbol</i></font>
  <font color="#000080"><b>let</b></font> fullName = <font color="#a61717">`</font>${first} ${last}<font color="#a61717">`</font>
  <font color="#000080"><b>const</b></font> ACTIVE_POST_COUNT = <font color="#0000FF">10</font>;
  <font color="#008800"><i>// ...</i></font>
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481551944746" ID="ID_1162619386" MODIFIED="1481551963718">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// multi line text also available</i></font>
<font color="#008800"><i>// new line characters will be preserved</i></font>
<font color="#000080"><b>let</b></font> userName = <font color="#0000FF">&quot;Sam&quot;</font>;
<font color="#000080"><b>let</b></font> admin = { fullName: <font color="#0000FF">&quot;Alex Williams&quot;</font> };

<font color="#000080"><b>let</b></font> veryLongText= <font color="#a61717">`</font>Hi ${userName},

<font color="#000080"><b>this</b></font> is a very
very

veeery
<font color="#000080"><b>long</b></font> text.
Regards,
  ${admin.fullName}
<font color="#a61717">`</font>;
<font color="#008800"><i>// print </i></font>
console.log( veryLongText );</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481552201910" FOLDED="true" ID="ID_1669334321" MODIFIED="1481552846794" TEXT="Array">
<node CREATED="1481552207309" FOLDED="true" ID="ID_1465978131" MODIFIED="1481552845308" TEXT="get values">
<node CREATED="1481552374530" ID="ID_1745190902" MODIFIED="1481552383334">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Get values from arrays </i></font>
<font color="#000080"><b>let</b></font> users = [<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];

<font color="#000080"><b>let</b></font> a = users[<font color="#0000FF">0</font>];
<font color="#000080"><b>let</b></font> b = users[<font color="#0000FF">1</font>];
<font color="#000080"><b>let</b></font> c = users[<font color="#0000FF">2</font>];
console.log(a, b, c); <font color="#008800"><i>// Sam Tyler Brook</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481552386681" ID="ID_944322483" MODIFIED="1481552411928">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// new value of array destructuring</i></font>
<font color="#000080"><b>let</b></font> users = [<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];
<font color="#000080"><b>let</b></font> [a, b, c] = users;
console.log(a, b , c) <font color="#008800"><i>// gives the same result</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481552419981" ID="ID_41577217" MODIFIED="1481552451755">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// to discard some values (use blank)</i></font>
<font color="#000080"><b>let</b></font> [a, , b] = users;
console.log(a, b); <font color="#008800"><i>// Sam Brook</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481552584234" ID="ID_55245128" MODIFIED="1481552587749">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// user rest params</i></font>
<font color="#000080"><b>let</b></font> users = [<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];
<font color="#000080"><b>let</b></font> [first, ...rest] = users;
console.log( first, rest ); <font color="#008800"><i>// Sam [&quot;Tyler&quot;, &quot;Brook&quot;]</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481552621292" ID="ID_206782796" MODIFIED="1481552625886">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// destructuring form return values</i></font>
<font color="#000080"><b>function</b></font> activeUsers() {
  <font color="#000080"><b>let</b></font> users = [<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Alex&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];
  <font color="#000080"><b>return</b></font> users;
}
<font color="#000080"><b>let</b></font> active = activeUsers();
console.log( active ); <font color="#008800"><i>// [&quot;Sam&quot;, &quot;Alex&quot;, &quot;Brook&quot;];</i></font>
<font color="#008800"><i>// in action</i></font>
<font color="#000080"><b>let</b></font> [a, b, c] = activeUsers();
console.log(a, b, c); Sam Alex Brook</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481552796482" FOLDED="true" ID="ID_1174273952" MODIFIED="1481552846524" TEXT="find">
<node CREATED="1481552814865" ID="ID_1045864063" MODIFIED="1481552818942">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Array.find</i></font>
<font color="#008800"><i>// returns first element which </i></font>
<font color="#008800"><i>// satisfies test function provided</i></font>
<font color="#000080"><b>let</b></font> users = [
  { login: <font color="#0000FF">&quot;Sam&quot;</font>,   admin: <font color="#000080"><b>false</b></font> },
  { login: <font color="#0000FF">&quot;Brook&quot;</font>, admin: <font color="#000080"><b>true</b></font>},
  { login: <font color="#0000FF">&quot;Tyler&quot;</font>, admin: <font color="#000080"><b>true</b></font>}
];

<font color="#000080"><b>let</b></font> admin = user.find( (user) =&gt; {
  <font color="#000080"><b>return</b></font> user.admin;
});
console.log(admin);  <font color="#008800"><i>// {&quot;login&quot;:&quot;Brook&quot;, admin: true }</i></font>

<font color="#008800"><i>// shorter version</i></font>
<font color="#000080"><b>let</b></font> admin = users.find( user =&gt; user.admin );
console.log(admin);  <font color="#008800"><i>// {&quot;login&quot;:&quot;Brook&quot;, admin: true }</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481552873314" FOLDED="true" ID="ID_337185680" MODIFIED="1481553529672" TEXT="Map">
<node CREATED="1481553411924" FOLDED="true" ID="ID_1933154218" MODIFIED="1481553528839" TEXT="usage">
<node CREATED="1481553321336" ID="ID_404902739" MODIFIED="1481553331486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Issues with objects </i></font>
<font color="#008800"><i>// it's keys converted to strings</i></font>
<font color="#000080"><b>let</b></font> user1 = { name: <font color="#0000FF">&quot;Sam&quot;</font> };
<font color="#000080"><b>let</b></font> user2 = { name: <font color="#0000FF">&quot;Tyler&quot;</font> };

<font color="#000080"><b>let</b></font> totalReplies = {};
totalReplies[user1] = <font color="#0000FF">5</font>;
totalReplies[user2] = <font color="#0000FF">42</font>;

console.log( Object.keys(totalReplies))
<font color="#008800"><i>// &gt; [&quot;[object Object]&quot;]</i></font>
<font color="#008800"><i>// so the last value we assign overrides all the values</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481553332586" ID="ID_1107527724" MODIFIED="1481553383484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// In maps keys and values can be anything </i></font>
<font color="#008800"><i>// and they not converted to strings</i></font>
<font color="#008800"><i>// setting and getting values from amp</i></font>
<font color="#000080"><b>let</b></font> user1 = { name: <font color="#0000FF">&quot;Sam&quot;</font> };
<font color="#000080"><b>let</b></font> user2 = { name: <font color="#0000FF">&quot;Tyler&quot;</font> };

<font color="#000080"><b>let</b></font> totalReplies = <font color="#000080"><b>new</b></font><b> Map();</b>
totalReplies.<b>set</b>( user1, <font color="#0000FF">5</font> );
totalReplies.<b>set</b>( user2, <font color="#0000FF">42</font> );

console.log( totalReplies.get(user1) ); <font color="#008800"><i>// 5</i></font>
console.log( totalReplies.get(user2) ); <font color="#008800"><i>// 42</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481553474836" ID="ID_1625043341" MODIFIED="1481553499320">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Map with for..of</i></font>
<font color="#000080"><b>let</b></font> mapSettings = <font color="#000080"><b>new</b></font> Map();
mapSettings.set( <font color="#0000FF">&quot;user&quot;</font>, <font color="#0000FF">&quot;Sam&quot;</font> );
mapSettings.set( <font color="#0000FF">&quot;user&quot;</font>, <font color="#0000FF">&quot;ES2015&quot;</font> );
mapSettings.set( <font color="#0000FF">&quot;user&quot;</font>, [<font color="#0000FF">&quot;Can't wait!&quot;</font>, <font color="#0000FF">&quot;So Cool&quot;</font>] );

<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> [key, value] of mapSettings) {
  console.log(<font color="#a61717">`</font>${key} = ${value}<font color="#a61717">`</font>);
}
<font color="#008800"><i>// &gt; user = Sam</i></font>
<font color="#008800"><i>// &gt; topic = ES2015</i></font>
<font color="#008800"><i>// &gt; replies = Can't wait!,So Cool</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481553415051" FOLDED="true" ID="ID_1779583820" MODIFIED="1481553504043">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Map vs Object
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1481553425177" ID="ID_890494985" MODIFIED="1481553468795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Exceptoin for using with maps</i></font>
<font color="#008800"><i>// use Map</i></font>
<font color="#000080"><b>let</b></font> recentPosts = <font color="#000080"><b>new</b></font> Map();

createPost(newPost, (data) =&gt; {
  <font color="#008800"><i>// keys unknown until runtime, so... Map!</i></font>
  recentPosts.set( data.author, data.message );
});

<font color="#008800"><i>// also use map when the key are the same type</i></font>
<font color="#008800"><i>// or values are the same type</i></font>
<font color="#008800"><i>// suppose data.author and data.message is the same</i></font>
socket.on(<font color="#0000FF">'new post'</font>, <font color="#000080"><b>function</b></font>(date) {
  recentPosts.set( data.author, data.message );
});

<font color="#008800"><i>// Use object</i></font>
<font color="#000080"><b>const</b></font> POSTS_PER_PAGE = <font color="#0000FF">15</font>;

<font color="#000080"><b>let</b></font> userSettings = {
  <font color="#008800"><i>// keys previosly defined, so... Object</i></font>
  <font color="#008800"><i>// also values are different types</i></font>
  perPage: POSTS_PER_PAGE,
  showRead: <font color="#000080"><b>true</b></font>,
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481553535409" FOLDED="true" ID="ID_404485783" MODIFIED="1481553769172" TEXT="WeakMap">
<node CREATED="1481553540742" ID="ID_36392102" MODIFIED="1481553576071">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// only objects can be used as keys</i></font>
<font color="#000080"><b>let</b></font> user = {};
<font color="#000080"><b>let</b></font> comment = {};

<font color="#000080"><b>let</b></font> mapSettings = <font color="#000080"><b>new</b></font> WeakMap():
mapSettings.set( user, <font color="#0000FF">&quot;user&quot;</font> );
mapSettings.set( comment, <font color="#0000FF">&quot;comment&quot;</font> );

console.log( mapSettings. get() );
console.log( mapSettings. get() );

mapSettings.set (<font color="#0000FF">&quot;title&quot;</font>, <font color="#0000FF">&quot;ES2015&quot;</font> );
<font color="#008800"><i>// &gt; Error Invalid value used as weak map key </i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553577755" ID="ID_92167043" MODIFIED="1481553671369">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// WeakMap require key</i></font>
<font color="#008800"><i>// WeakMap is not iterable</i></font>
<font color="#000080"><b>let</b></font> user = {};

<font color="#000080"><b>let</b></font> mapSettings = <font color="#000080"><b>new</b></font> WeakMap();
mapSettings.set( user, <font color="#0000FF">&quot;ES2015&quot;</font> );

console.log( mapSettings.get(user) );    <font color="#008800"><i>// ES2015</i></font>
console.log( mapSettings.has(user) );    <font color="#008800"><i>// true</i></font>
console.log( mapSettings.<font color="#000080"><b>delete</b></font>(user) ); <font color="#008800"><i>// true</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553674555" ID="ID_831144944" MODIFIED="1481553761296">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// WeakMap is better with memory</i></font>
<font color="#000080"><b>let</b></font> user = {};
lett userStatus = <font color="#000080"><b>new</b></font> WeakMap();
<font color="#008800"><i>// Object reference passed as key to the WeakMap</i></font>
userStatus.set( user, <font color="#0000FF">&quot;logged&quot;</font> );

<font color="#008800"><i>// ...</i></font>
someOtherFunction( user );
<font color="#008800"><i>// after it returns user can be garbage collected</i></font>
<font color="#008800"><i>// Weak maps don't prevent garbage collector from </i></font>
<font color="#008800"><i>// collecting objects currently used as keys if </i></font>
<font color="#008800"><i>// they no longer refferenced anywhere else</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481553770814" FOLDED="true" ID="ID_620126245" MODIFIED="1481553895492" TEXT="Set">
<node CREATED="1481553809313" ID="ID_1762665160" MODIFIED="1481553821264">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// limitatois of Arrays</i></font>
<font color="#000080"><b>let</b></font> tags = [];

tags.push( <font color="#0000FF">&quot;JavaScript&quot;</font> );
tags.push( <font color="#0000FF">&quot;Programming&quot;</font> );
tags.push( <font color="#0000FF">&quot;Web&quot;</font> );
tags.push( <font color="#0000FF">&quot;Web&quot;</font> );

console.log( <font color="#0000FF">&quot;Total items&quot;</font>, tags.length )
<font color="#008800"><i>// &gt; total items 4</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553822296" ID="ID_608364547" MODIFIED="1481553847094">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Set store unique values of any type</i></font>
<font color="#008800"><i>// add method to add values to Set</i></font>
<font color="#000080"><b>let</b></font> tags = <font color="#000080"><b>new</b></font> Set();

tags.add( <font color="#0000FF">&quot;JavaScript&quot;</font> );
tags.add( <font color="#0000FF">&quot;Programming&quot;</font> );
tags.add({ version: <font color="#0000FF">&quot;2015&quot;</font> });
tags.add( <font color="#0000FF">&quot;Web&quot;</font> );
<font color="#008800"><i>// Duplicate entries are ignored</i></font>
tags.add( <font color="#0000FF">&quot;Web&quot;</font> );

console.log( <font color="#0000FF">&quot;Total items&quot;</font>, tags.size );
<font color="#008800"><i>// &gt; total items 4</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553849680" ID="ID_31673825" MODIFIED="1481553869101">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Set are iterable</i></font>
<font color="#000080"><b>let</b></font> tags = <font color="#000080"><b>new</b></font> Set();
<font color="#008800"><i>// ...</i></font>
<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> tag of tags){
  console.log(tag);
}
<font color="#008800"><i>// &gt; JavaScript</i></font>
<font color="#008800"><i>// &gt; Programming</i></font>
<font color="#008800"><i>// &gt; {version: '2015'}</i></font>
<font color="#008800"><i>// &gt; Web</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553870718" ID="ID_742416145" MODIFIED="1481553890681">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Set is destructuring</i></font>
<font color="#000080"><b>let</b></font> [a, b , c, d] = tags.
console.log(a, b, c, d);
<font color="#008800"><i>// &gt; JavaScript Programming {version: '2015'} Web</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481553774684" FOLDED="true" ID="ID_576234254" MODIFIED="1481554151553" TEXT="WeakSet">
<node CREATED="1481553897562" ID="ID_1164142145" MODIFIED="1481553935454">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// more memory efficient type</i></font>
<font color="#008800"><i>// only objects could be assigned</i></font>
<font color="#000080"><b>let</b></font> weakTags = <font color="#000080"><b>new</b></font> WeakSet();
weakTags.add(<font color="#0000FF">&quot;JavaScript&quot;</font>);  
<font color="#008800"><i>// &gt; TypeError: Invalid values used in weak set</i></font>
weakTags.add({ name: <font color="#0000FF">&quot;JavaScript&quot;</font> });
<font color="#000080"><b>let</b></font> iOS = { name: <font color="#0000FF">&quot;iOS&quot;</font> };
weakTags.add(iOS);</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481553940809" ID="ID_1181140924" MODIFIED="1481554014118">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// WeakSet has new methods</i></font>
weakTags.has(iOS);    <font color="#008800"><i>// &gt; true</i></font>
weakTags.<font color="#000080"><b>delete</b></font>(iOS); <font color="#008800"><i>// &gt; true</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481554016007" ID="ID_682422373" MODIFIED="1481554032705">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// WeakSet can't be used with for..of</i></font>
<font color="#000080"><b>for</b></font> (<font color="#000080"><b>let</b></font> wt of weakTags){
  console.log(wt) 
<font color="#008800"><i>// &gt; TypeError weakTags[Symbol.iterator]</i></font>
<font color="#008800"><i>// &gt; is not a function</i></font>
}
<font color="#008800"><i>// can't return values from it</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481554054281" FOLDED="true" ID="ID_221219636" MODIFIED="1481554150766" TEXT="usage example">
<node CREATED="1481554064699" ID="ID_1627222330" MODIFIED="1481554096090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Practical usage of WeakSet</i></font>
<font color="#008800"><i>// Using WeakSets to Show Unread Posts</i></font>
<font color="#008800"><i>// In JavaScript try to use </i></font>
<font color="#008800"><i>// as much immutable code as you could</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481554097820" ID="ID_1810931119" MODIFIED="1481554117653">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// old style sample</i></font>
<font color="#008800"><i>// ... when post is clicked on</i></font>
<font color="#000080"><b>let</b></font> post = { <font color="#008800"><i>/* ... */</i></font> };
<font color="#008800"><i>// when post is clicked on</i></font>
postList.addEventListener(<font color="#0000FF">'click'</font>, (event) =&gt; {
  <font color="#008800"><i>// ...</i></font>
  post.isRead = <font color="#000080"><b>true</b></font>; 
  <font color="#008800"><i>// ^ mutates each post object uneccessarily</i></font>
});

<font color="#008800"><i>// ... renderind posts</i></font>
<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> post of postArray){
  <font color="#008800"><i>// check on each post </i></font>
  <font color="#000080"><b>if</b></font> (!post.isRead) { 
    _addNewPostClass(post.element);
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481554118701" ID="ID_664181378" MODIFIED="1481554139881">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// new style</i></font>
<font color="#000080"><b>let</b></font> readPosts = <font color="#000080"><b>new</b></font> WeakSet();

<font color="#008800"><i>// ... when post is clicked on </i></font>
postList.addEventListener(<font color="#0000FF">'click'</font>, (event) =&gt; {
  <font color="#008800"><i>// ...</i></font>
  readPosts.add(post);
});

<font color="#008800"><i>// ...rendering posts</i></font>
<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> post of postArray){
  <font color="#000080"><b>if</b></font>(!readPosts.has(post)){
    <font color="#008800"><i>// no unexpected side effects</i></font>
    _addNewPostClass(post);
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481556219901" FOLDED="true" ID="ID_442185546" MODIFIED="1481556717453" TEXT="Promise">
<node CREATED="1481556226156" FOLDED="true" ID="ID_166668350" MODIFIED="1481556313027" TEXT="old approach">
<node CREATED="1481556257027" ID="ID_1273832013" MODIFIED="1481556283307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// syncronous style functon wait for return values</i></font>
<font color="#008800"><i>// freezeing keyboard and mouse interactions</i></font>
<font color="#000080"><b>let</b></font> results = getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>);
<font color="#008800"><i>//            ^ page freeze until value returned form server</i></font>
ui.renderSidebar(results);</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481556248538" FOLDED="true" ID="ID_90165509" MODIFIED="1481556354132" TEXT="old style async">
<node CREATED="1481556289943" ID="ID_910565182" MODIFIED="1481556352020">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// fix this by writing asyncronious style functions</i></font>
getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>, <font color="#000080"><b>function</b></font> (results) {
  ui.renderSidebar(results);
})
<font color="#008800"><i>// ^ this style is callilng continuation-passing-style(CPS) </i></font>
<font color="#008800"><i>// - async programiing. tell a function how to continue execution</i></font>
<font color="#008800"><i>//   by passing callbacks. Caveat: complicated nested code</i></font>

<font color="#008800"><i>// old style</i></font>
<font color="#008800"><i>// Hard to read and needs check for error on all callbacks</i></font>
getPoolResultsFromServer(poolName, <font color="#000080"><b>function</b></font> (error, results) {
  <font color="#000080"><b>if</b></font> (error) { <font color="#008800"><i>/* handle error */</i></font> }
  ui.renderSidebar(results, <font color="#000080"><b>function</b></font> (error) {
    <font color="#000080"><b>if</b></font>(error) { <font color="#008800"><i>/*handle error*/</i></font> }
    sendNotificationToServer(poolName, results, <font color="#000080"><b>function</b></font> (error, response) {
      <font color="#000080"><b>if</b></font>(error){ <font color="#008800"><i>/* handle error */</i></font> }
      doSomethingElseNonBlocking(response, <font color="#000080"><b>function</b></font> (error) {
        <font color="#000080"><b>if</b></font>(error) { <font color="#008800"><i>/* handle error */</i></font> }
        <font color="#008800"><i>// ....</i></font>
      });
    });
  });
});</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481556355212" FOLDED="true" ID="ID_1345571288" MODIFIED="1481556705843" TEXT="Promise style">
<node CREATED="1481556370331" ID="ID_428616371" MODIFIED="1481556389345">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// new (Promise) style</i></font>
getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>)
  .then(ui.renderSidebar)
  .then(sendNotificationToServer)
  .then(doSomethingElseNonBlocking)
  .<font color="#000080"><b>catch</b></font>(<font color="#000080"><b>function</b></font>(error){
    conlose.log(<font color="#0000FF">&quot;error: &quot;</font>, error);
  });</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556392200" ID="ID_1588154620" MODIFIED="1481556408222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Promise constructor</i></font>
<font color="#000080"><b>function</b></font> getPoolResultsFromServer(poolName) { 
  
  <font color="#000080"><b>return</b></font> <font color="#000080"><b>new</b></font> Promise(<font color="#000080"><b>function</b></font> (resolve, reject) {
  <font color="#008800"><i>//                 ^ takes 2 callbacks which </i></font>
  <font color="#008800"><i>//                   responsible for resolving</i></font>
  <font color="#008800"><i>//                   or rejecting the Promise</i></font>

    <font color="#008800"><i>// called when non-blocking is done execution</i></font>
    resolve(someValue);

    <font color="#008800"><i>// called when error occurs</i></font>
    reject(someValue);
  })
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556432789" ID="ID_1197184093" MODIFIED="1481556443771">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Promise Lifecycle</i></font>
<font color="#008800"><i>// at begining it jas pending states</i></font>
<font color="#008800"><i>// then it can become fulfilled(resolved) OR rejected</i></font>
<font color="#008800"><i>// Promise represent a future (possible) value</i></font>
<font color="#008800"><i>// eventual result of asynchoronous operation</i></font>
<font color="#000080"><b>function</b></font> getPoolResultsFromServer(poolName){

  <font color="#000080"><b>return</b></font> <font color="#000080"><b>new</b></font> Promise(<font color="#000080"><b>function</b></font> (resolve, reject) {
    <font color="#000080"><b>let</b></font> url = <font color="#a61717">`</font>/result/${poolName}<font color="#a61717">`</font>;
    <font color="#000080"><b>let</b></font> request = <font color="#000080"><b>new</b></font> XMLHttpRequest();
    request.open(<font color="#0000FF">'GET'</font>, url, <font color="#000080"><b>true</b></font>);
    request.onload = <font color="#000080"><b>function</b></font>() {
      <font color="#000080"><b>if</b></font> (request.state &gt;= <font color="#0000FF">200</font> &amp;&amp; request.status &lt; <font color="#0000FF">400</font>) {
        <font color="#008800"><i>// here call of resolve moves Promise</i></font>
        <font color="#008800"><i>// to fullfiled state &amp; can use result</i></font>
        <font color="#008800"><i>// in method .then</i></font>
        resolve(JSON.parse(request.response));
        <font color="#008800"><i>// ^ call resolve upon a successful response</i></font>
      }
    };
    <font color="#008800"><i>// </i></font>
    request.send
  })
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556488941" ID="ID_1980508028" MODIFIED="1481556520448">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Read the results of fullfiled Promise</i></font>
<font color="#000080"><b>let</b></font> fetchingRestults = getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>);
fetchingRestults.then(<font color="#000080"><b>function</b></font> (results) {
  <font color="#008800"><i>//                  ^         ^ getting results here from Promise</i></font>
  <font color="#008800"><i>//                  | Method only be invoked once Promise resolved</i></font>
  ui.renderSidebar(results);
});

<font color="#008800"><i>// we can get rid of fetchingResults function</i></font>
getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>)
  .then(<font color="#000080"><b>function</b></font> (results) {
    ui.renderSidebar(results);
  });</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556496642" ID="ID_1162233119" MODIFIED="1481556535208">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Chaining multiple promises</i></font>
getPoolResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>)
  .then(<font color="#000080"><b>function</b></font> (results) {
    <font color="#000080"><b>return</b></font> results.filter((result) =&gt; result.city === <font color="#0000FF">&quot;Orlando&quot;</font>);
    <font color="#008800"><i>// ^ returns pools from orlando and </i></font>
    <font color="#008800"><i>//   push this value to antoher then</i></font>
  })
  .then(<font color="#000080"><b>function</b></font> (resultsFromOrlando) {
   <font color="#008800"><i>//            ^ last return values becomes</i></font>
   <font color="#008800"><i>//              argument to the following</i></font>
   <font color="#008800"><i>//              then funcittion(this function)</i></font>
    ui.renderSidebar(resultsFromOrlando);
  });</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556551495" ID="ID_882291487" MODIFIED="1481556590489">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// rejecting Promise</i></font>
<font color="#000080"><b>function</b></font> getPollResultsFromServer(pollName) {
  
  <font color="#000080"><b>return</b></font> <font color="#000080"><b>new</b></font> Promise(<font color="#000080"><b>function</b></font> (resolve, reject) {

    <font color="#008800"><i>// ...</i></font>
    request.onload = <font color="#000080"><b>function</b></font>() {
      <font color="#000080"><b>if</b></font>(request.status &gt;= <font color="#0000FF">200</font> &amp;&amp; request.status &lt; <font color="#0000FF">400</font>) {
        resolve(request.response);
      } <font color="#000080"><b>else</b></font> {
        reject(<font color="#000080"><b>new</b></font> Error(request.status));
      }
    }
    request.onerror = <font color="#000080"><b>function</b></font> () {
      reject(<font color="#000080"><b>new</b></font> Error(<font color="#0000FF">&quot;Error Fetching Results&quot;</font>));
      <font color="#008800"><i>//          ^ call reject with Error object passing</i></font>
    }
  })
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556592332" ID="ID_582001852" MODIFIED="1481556626241">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// when error ocurrs execution goes to the catch() function</i></font>
getPollResultsFromServer(<font color="#0000FF">&quot;Sass vs. Less&quot;</font>)
  .then(<font color="#000080"><b>function</b></font> (results) {
    <font color="#000080"><b>return</b></font> results.filter((result) =&gt; result.city === <font color="#0000FF">&quot;Orlando&quot;</font>);
    <font color="#008800"><i>// ...</i></font>
  })
  .then(<font color="#000080"><b>function</b></font> (result) {
    <font color="#008800"><i>// ..</i></font>
  })
  <font color="#008800"><i>// reject function parameter goest </i></font>
  <font color="#008800"><i>// as attribute to catch function</i></font>
  .<font color="#000080"><b>catch</b></font>(<font color="#000080"><b>function</b></font> (error) {
    console.log(<font color="#0000FF">&quot;Error: &quot;</font>, error);
  })</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556631121" ID="ID_1609163070" MODIFIED="1481556691197">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// passing functions as arguments</i></font>
<font color="#000080"><b>function</b></font> filetedResults(result) {
  <font color="#008800"><i>// body...</i></font>
}

<font color="#000080"><b>let</b></font> ui = {
  renderSidebar(filetedResults) { <font color="#008800"><i>/*  */</i></font> }
}


getPollResultsFromServer(<font color="#0000FF">&quot;sass vs. Less&quot;</font>)
  .then(filetedResults)
  .then(ui.renderSidebar)
  .<font color="#000080"><b>catch</b></font>(<font color="#000080"><b>function</b></font> (error) {
    console.log(<font color="#0000FF">&quot;Error: &quot;</font>, error);
  });</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1481552702767" FOLDED="true" ID="ID_1591388040" MODIFIED="1481556890523" TEXT="loops">
<node COLOR="#0033ff" CREATED="1481552705961" FOLDED="true" ID="ID_924654442" MODIFIED="1481552750919" TEXT="for..in">
<node CREATED="1481552726122" ID="ID_595237560" MODIFIED="1481552746986">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// old style loop for..in</i></font>
<font color="#008800"><i>// uses index to read the </i></font>
<font color="#000080"><b>let</b></font> names = =[<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];
<font color="#000080"><b>for</b></font> (<font color="#000080"><b>let</b></font> index <font color="#000080"><b>in</b></font> names) {
  console.log( names[index] );
}
<font color="#008800"><i>// &gt; Sam Tyler Brook</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node COLOR="#0033ff" CREATED="1481552719124" FOLDED="true" ID="ID_1272592544" MODIFIED="1481552781182" TEXT="for..of">
<node CREATED="1481552752246" ID="ID_1199355102" MODIFIED="1481552771645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// for of loop no need to look for index anymore</i></font>
<font color="#000080"><b>for</b></font> (<font color="#000080"><b>let</b></font> name of names) {
  console.log( name );
}
<font color="#008800"><i>// &gt; Sam Tyler Brook</i></font>
<font color="#008800"><i>// for of cant be used with objects</i></font>
<font color="#008800"><i>// to check if it's available need to check for object</i></font>
<font color="#008800"><i>// to have Symbol.iterator -&gt; the object is iterable </i></font>
<font color="#000080"><b>typeof</b></font> names[Symbol.iterator] === <font color="#0000FF">'function'</font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
<node CREATED="1481554728437" FOLDED="true" ID="ID_1159847485" MODIFIED="1481556890523" TEXT="modules">
<node CREATED="1481554731565" FOLDED="true" ID="ID_1275287953" MODIFIED="1481555074377" TEXT="old approach">
<node CREATED="1481554792719" ID="ID_1235079800" MODIFIED="1481554803589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Old script tag invoking and global space using </i></font>
<font color="#008800"><i>// modules were strugling with potential and </i></font>
<font color="#008800"><i>// unexpected side effed cts</i></font>
<font color="#000080"><b>let</b></font> element = $(<font color="#0000FF">&quot;...&quot;</font>).find(...);
<font color="#000080"><b>let</b></font> filtered = _.each(...);
flashMessage(<font color="#0000FF">&quot;Hello&quot;</font>);</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481554806872" FOLDED="true" ID="ID_22597218" MODIFIED="1481555732358" TEXT="using">
<node COLOR="#0033ff" CREATED="1481555055318" FOLDED="true" ID="ID_1542499862" MODIFIED="1481555729320" TEXT="default">
<node CREATED="1481554828269" ID="ID_394645867" MODIFIED="1481554850300">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Exporting</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#008800"><i>// export exposes function to module system</i></font>
<font color="#000080"><b>export</b></font> <font color="#000080"><b>default</b></font> <font color="#000080"><b>function</b></font>(message){
  <font color="#008800"><i>//   ^ type export is the simples way of export</i></font>
  <font color="#008800"><i>//     it alows only single export</i></font>
  alert(message);
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481554874192" ID="ID_1297234796" MODIFIED="1481554884841">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Importing</i></font>
<font color="#008800"><i>// app.js</i></font>
<font color="#000080"><b>import</b></font> flashMessage from <font color="#0000FF">'./flash-message'</font>;
<font color="#008800"><i>//     ^                   ^ should be on the same</i></font>
<font color="#008800"><i>//     |                     folder</i></font>
<font color="#008800"><i>//      because defalut were used we could</i></font>
<font color="#008800"><i>//      name our imported object as we want</i></font>
flashMessage(<font color="#0000FF">&quot;Hello&quot;</font>);
<font color="#008800"><i>// ...</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481554974407" ID="ID_1718333624" MODIFIED="1481554977403">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%">// Run code
// index.html
<font color="#008080">&lt;!DOCTYPE html&gt;</font>
<font color="#000080"><b>&lt;body&gt;</b></font>
  <font color="#000080"><b>&lt;script&gt;</b></font>
    <font color="#008800"><i>// It should be importet with scipt but doesn't polute global namespace </i></font>
  <font color="#000080"><b>&lt;/script&gt;</b></font> 
  <font color="#000080"><b>&lt;script </b></font><font color="#FF0000">src=</font><font color="#0000FF">&quot;./flash-message.js&quot;</font><font color="#000080"><b>&gt;&lt;/script&gt;</b></font>
  <font color="#000080"><b>&lt;script </b></font><font color="#FF0000">src=</font><font color="#0000FF">&quot;./app.js&quot;</font><font color="#000080"><b>&gt;&lt;/script&gt;</b></font>
<font color="#000080"><b>&lt;/body&gt;</b></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481555031445" ID="ID_1318638391" MODIFIED="1481555049652">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Declaring isn't exporting</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#000080"><b>export</b></font> <font color="#000080"><b>default</b></font> <font color="#000080"><b>function</b></font>(message){
  <font color="#008800"><i>//   ^ type export is the simples way of export</i></font>
  alert(message);
}
<font color="#008800"><i>// not available outside module</i></font>
<font color="#000080"><b>function</b></font> logMessage(message) {
  console.log(message);
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481555060876" FOLDED="true" ID="ID_1103306551" MODIFIED="1481555732020" TEXT="named">
<node CREATED="1481555102254" ID="ID_1096663192" MODIFIED="1481555104711">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Export</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#008800"><i>// both functions exported</i></font>
<font color="#000080"><b>export</b></font> <font color="#000080"><b>function</b></font> alertMessage(message){
  alert(message);
}
<font color="#000080"><b>export</b></font> <font color="#000080"><b>function</b></font> logMessage(message) {
  console.log(message);
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555107204" ID="ID_1642370062" MODIFIED="1481555131304">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Import </i></font>
<font color="#008800"><i>// app.js</i></font>
<font color="#000080"><b>import</b></font> { alertMessage, logMessage } from <font color="#0000FF">'./flash-message'</font>;
<font color="#008800"><i>//              ^names   ^here must match</i></font>
alertMessage(<font color="#0000FF">'Hello from alert'</font>);
logMessage(<font color="#0000FF">'Hello from log'</font>);</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555230191" ID="ID_1013720261" MODIFIED="1481555253143">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Import as an Object</i></font>
<font color="#008800"><i>// app.js</i></font>
<font color="#000080"><b>import</b></font> * as flash from <font color="#0000FF">'./flash-message'</font>;
<font color="#008800"><i>//          ^functions from exports become object properties</i></font>
flash.alertMessage(<font color="#0000FF">'Hello from alert'</font>);
<font color="#008800"><i>// ^ call as object method call</i></font>
flash.logMessage(<font color="#0000FF">'Hello from log'</font>);</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555257371" ID="ID_1681398311" MODIFIED="1481555277157">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Better Export</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#000080"><b>function</b></font> alertMessage(message){
  alert(message);
}
<font color="#000080"><b>function</b></font> logMessage(message) {
  console.log(message);
}

<font color="#000080"><b>export</b></font> { alertMessage, logMessage }</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481555406280" FOLDED="true" ID="ID_1831200010" MODIFIED="1481555724077" TEXT="with constants">
<node CREATED="1481555414830" ID="ID_1603552900" MODIFIED="1481555441746">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Modules with classes and constants</i></font>
<font color="#008800"><i>// Extracting hardcoded constants</i></font>
<font color="#008800"><i>// constants only worked for scoped module</i></font>
<font color="#008800"><i>// The way to fix that is to created module with</i></font>
<font color="#008800"><i>// only a constants</i></font>
<font color="#008800"><i>// constants.js</i></font>
<font color="#000080"><b>export</b></font> <font color="#000080"><b>const</b></font> MAX_USERS = <font color="#0000FF">3</font>;
<font color="#000080"><b>export</b></font> <font color="#000080"><b>const</b></font> MAX_REPLIES = <font color="#0000FF">3</font>;
<font color="#008800"><i>// OR</i></font>
<font color="#000080"><b>const</b></font> MAX_USERS = <font color="#0000FF">3</font>;
<font color="#000080"><b>const</b></font> MAX_REPLIES = <font color="#0000FF">3</font>;
<font color="#000080"><b>export</b></font> { MAX_USERS, MAX_REPLIES };</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555443968" ID="ID_1409280449" MODIFIED="1481555460533">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// load-profiles.js</i></font>
<font color="#000080"><b>import</b></font> { MAX_USERS, MAX_REPLIES } form <font color="#0000FF">'./constants'</font>

<font color="#000080"><b>function</b></font> loadProfiles(userNames) {
  
  <font color="#000080"><b>if</b></font> (userNames.length &gt; MAX_USERS) {
    <font color="#008800"><i>// ...</i></font>
  }

  <font color="#000080"><b>if</b></font> (someElement &gt; MAX_REPLIES) {
    <font color="#008800"><i>// ...</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555463611" ID="ID_783220621" MODIFIED="1481555480354">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// list-replies.js</i></font>
<font color="#000080"><b>import</b></font> { MAX_REPLIES } form <font color="#0000FF">'./constants'</font>

<font color="#000080"><b>function</b></font> listREplies(replies = []) {
  <font color="#000080"><b>if</b></font> (replies.length &gt; MAX_REPLIES) {
    <font color="#008800"><i>// ...</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555492591" ID="ID_1556803829" MODIFIED="1481555497386">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// display-watchers.js</i></font>
<font color="#000080"><b>import</b></font> {MAX_USERS} from <font color="#0000FF">'./constants'</font>

<font color="#000080"><b>function</b></font> displayWatchers(watchers = [ ]) {
  <font color="#000080"><b>if</b></font> (watchers.length &gt; MAX_USERS) {
    <font color="#008800"><i>// ...</i></font>
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481555504764" FOLDED="true" ID="ID_1154186921" MODIFIED="1481555725225" TEXT="with object">
<node COLOR="#0033ff" CREATED="1481555614319" FOLDED="true" ID="ID_1792212771" MODIFIED="1481555717980" TEXT="default">
<node CREATED="1481555512414" ID="ID_1244463096" MODIFIED="1481555569500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// default export</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#000080"><b>export</b></font> defalut <font color="#000080"><b>class</b></font> FlashMessage { 
<font color="#008800"><i>//      ^ because we use default we could assing</i></font>
<font color="#008800"><i>//        any variable name when import</i></font>
  constructor(message) {
    <font color="#000080"><b>this</b></font>.message = message;
  }

  renderAlert() {
    alert (<font color="#a61717">`</font>${<font color="#000080"><b>this</b></font>.message} from alert<font color="#a61717">`</font>);
  }

  renderLog(){
    console.log(<font color="#a61717">`</font>${<font color="#000080"><b>this</b></font>.message} from log<font color="#a61717">`</font>);
  }
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
<node CREATED="1481555575236" ID="ID_1793542738" MODIFIED="1481555602103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// app.js</i></font>
<font color="#000080"><b>import</b></font> FlashMessage from <font color="#0000FF">'./flash-message'</font>
<font color="#008800"><i>//     ^ F capitalized because we import class</i></font>

<font color="#000080"><b>let</b></font> flash = <font color="#000080"><b>new</b></font> FlashMessage(<font color="#0000FF">&quot;Hello&quot;</font>);
flash.renderAlert();
flash.logMessage();</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me --></richcontent>
</node>
</node>
<node CREATED="1481555635425" FOLDED="true" ID="ID_1387617572" MODIFIED="1481555718430" TEXT="named">
<node CREATED="1481555665306" ID="ID_43248421" MODIFIED="1481555668435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// named export</i></font>
<font color="#008800"><i>// flash-message.js</i></font>
<font color="#000080"><b>class</b></font> FlashMessage {
  constructor(message) {
    <font color="#000080"><b>this</b></font>.message = message;
  }

  renderAlert() {
    alert(<font color="#a61717">`</font>${<font color="#000080"><b>this</b></font>.message} form alert<font color="#a61717">`</font>);
  }

  renderLog(message){
    console.log(<font color="#a61717">`</font>${<font color="#000080"><b>this</b></font>.message} from log<font color="#a61717">`</font>);
  }
}

<font color="#000080"><b>export</b></font> { FlashMessage }</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481555672776" ID="ID_331907114" MODIFIED="1481555694356">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// import</i></font>
<font color="#008800"><i>// app.js</i></font>
<font color="#008800"><i>// when using named export the import script needs</i></font>
<font color="#008800"><i>// assign imported class into variable with the</i></font>
<font color="#008800"><i>// same name as the class</i></font>
<font color="#000080"><b>import</b></font> { FlashMessage } from <font color="#0000FF">'./flash-message'</font>

<font color="#000080"><b>let</b></font> flash = <font color="#000080"><b>new</b></font> FlashMessage(<font color="#0000FF">&quot;Hello&quot;</font>);
flash.renderAlert();
flash.logMessage();</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1481556727847" FOLDED="true" ID="ID_1411524341" MODIFIED="1481556890522" TEXT="iterators">
<node COLOR="#0033ff" CREATED="1481556731560" FOLDED="true" ID="ID_1087308314" MODIFIED="1481556792242" TEXT="Array">
<node CREATED="1481556774984" ID="ID_1611268330" MODIFIED="1481556787249">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// arrays are iterables</i></font>
<font color="#008800"><i>// iterables return iterator - object can</i></font>
<font color="#008800"><i>// access items from a collection 1 at time</i></font>
<font color="#008800"><i>// and keeping track of its current position</i></font>
<font color="#000080"><b>let</b></font> names = [<font color="#0000FF">&quot;Sam&quot;</font>, <font color="#0000FF">&quot;Tyler&quot;</font>, <font color="#0000FF">&quot;Brook&quot;</font>];
<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> name of names)
  <font color="#008800"><i>// ^ befind the scene </i></font>
       <font color="#000080"><b>let</b></font> iterator = name[Symbol.iterator]();

       <font color="#000080"><b>let</b></font> firstRun = iterator.next();
  <font color="#008800"><i>//                           ^ the result o next method</i></font>
  <font color="#008800"><i>//                             { done:false, value: &quot;Sam&quot; }</i></font>
       <font color="#000080"><b>let</b></font> name = firstRun.value;

       <font color="#000080"><b>let</b></font> secondRun = iterator.next();
       <font color="#000080"><b>let</b></font> name = secondRun.value();

       <font color="#000080"><b>let</b></font> thirdRun = iterator.next();
       <font color="#000080"><b>let</b></font> name = thirdRun.value;
  <font color="#008800"><i>//   Let's look into last iterator element;</i></font>
       <font color="#000080"><b>let</b></font> forthRun = iterator.next();
  <font color="#008800"><i>//       ^ { done: true, value: undefined }</i></font>
  <font color="#008800"><i>//   end</i></font>
  console.log( name );
}
<font color="#008800"><i>// done(boolean) return false it iterator able to iterate</i></font>
<font color="#008800"><i>// return true if iterator is past the end of the collection</i></font>

<font color="#008800"><i>// value(any) - any value, but when </i></font>
<font color="#008800"><i>// done: true it returns undefined</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481556792534" FOLDED="true" ID="ID_190049711" MODIFIED="1481556815529" TEXT="create">
<node CREATED="1481556795684" ID="ID_1646531313" MODIFIED="1481556813417">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// create iterator</i></font>
<font color="#000080"><b>let</b></font> post = { <font color="#008800"><i>/*.... */</i></font> };

post[Symbol.iterator] = <font color="#000080"><b>function</b></font> () {
  <font color="#000080"><b>let</b></font> properties = Object.keys(<font color="#000080"><b>this</b></font>);
  <font color="#008800"><i>//                ^ return array with object</i></font>
  <font color="#008800"><i>//                  properties</i></font>
  <font color="#000080"><b>let</b></font> count = <font color="#0000FF">0</font>;
  <font color="#008800"><i>//          ^ allows us to access the properties array</i></font>
  <font color="#008800"><i>//            by index</i></font>
  <font color="#000080"><b>let</b></font> isDone = <font color="#000080"><b>false</b></font>;
  <font color="#008800"><i>//           ^ will be set to true when we are done</i></font>
  <font color="#008800"><i>//             with the loop</i></font>
  <font color="#000080"><b>let</b></font> next = () =&gt; {
    <font color="#000080"><b>if</b></font> (count &gt;= properties.length) {
      <font color="#008800"><i>//   ^ ends the loop after reaching </i></font>
      <font color="#008800"><i>//     the last porperty</i></font>
      isDone = <font color="#000080"><b>true</b></font>;
    }
    <font color="#000080"><b>return</b></font> { done: isDone, value: <font color="#000080"><b>this</b></font>[properties[count++]] };
    <font color="#008800"><i>//                            ^ reffers to the post object</i></font>
  }

  <font color="#000080"><b>return</b></font>  { next };
}</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
<node CREATED="1481556816586" FOLDED="true" ID="ID_148259632" MODIFIED="1481556876324" TEXT="using">
<node CREATED="1481556837984" ID="ID_559199389" MODIFIED="1481556840101">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Using iterator</i></font>
<font color="#000080"><b>let</b></font> post = {
  title: <font color="#0000FF">&quot;New Features in JS&quot;</font>,
  replies: <font color="#0000FF">19</font>
};

<font color="#000080"><b>for</b></font>(<font color="#000080"><b>let</b></font> p of post) {
  console.log(p);
}
<font color="#008800"><i>// &gt; New Features in JS</i></font>
<font color="#008800"><i>// &gt; 19 </i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556841134" ID="ID_1017129304" MODIFIED="1481556859202">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Using sread operator</i></font>
<font color="#000080"><b>let</b></font> values = [...post];
console.log( values );
<font color="#008800"><i>// &gt; ['New Features in JS', 19]</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
<node CREATED="1481556861271" ID="ID_1764144744" MODIFIED="1481556874885">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div style="background-color: #ffffff; background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0">
      <pre style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; line-height: 125%"><font color="#008800"><i>// Iterables with destructuring</i></font>
<font color="#000080"><b>let</b></font> [title, replies] = post;
console.log( title );
console.log( replies );
<font color="#008800"><i>// &gt; New Features in JS</i></font>
<font color="#008800"><i>// &gt; 19</i></font>
</pre>
    </div>
  </body>
</html>
<!-- HTML generated using hilite.me -->
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1481214654026" ID="ID_1836000205" MODIFIED="1481214657108" POSITION="left" TEXT="ES2016"/>
<node CREATED="1419328530685" FOLDED="true" ID="ID_1880140087" MODIFIED="1481019650818" POSITION="left" TEXT="info">
<node CREATED="1351605948750" FOLDED="true" ID="ID_1071224225" MODIFIED="1480681866794" TEXT="Tips">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1351605953255" ID="ID_1441700475" LINK="http://caniuse.com/" MODIFIED="1480676982772" TEXT="canIUse"/>
</node>
<node CREATED="1418754270898" FOLDED="true" ID="ID_744461084" MODIFIED="1480681865105" TEXT="glossary">
<node CREATED="1418754278733" FOLDED="true" ID="ID_269002831" MODIFIED="1480676976787" TEXT="AMD">
<node CREATED="1439202246858" ID="ID_545800283" MODIFIED="1439202254915" TEXT="Asyncronuous module definition"/>
<node COLOR="#0033ff" CREATED="1418754280890" ID="ID_1520558113" LINK="https://ru.wikipedia.org/wiki/Asynchronous_module_definition" MODIFIED="1480676916420" TEXT="wiki-ru"/>
</node>
<node CREATED="1354632592619" FOLDED="true" ID="ID_783375115" MODIFIED="1480676978587" TEXT="closure">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354632597951" ID="ID_455825447" MODIFIED="1439202182181">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">// Whenever you see the function keyword within another function, the inner function has access to variables in the outer function. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">function foo(x) { </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">var tmp = 3;&#xa0; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">function bar(y) {&#xa0;&#xa0;&#xa0; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">alert(x + y + (++tmp));&#xa0; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">}&#xa0; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">bar(10); </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">} </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">foo(2) </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">This will always alert 16, because&#xa0;bar&#xa0;can access the&#xa0;x&#xa0;which was defined as an argument to&#xa0;foo, and it can also access&#xa0;tmp&#xa0;from&#xa0;foo. That is&#xa0;not&#xa0;a closure. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">A closure is when you&#xa0;return&#xa0;the inner function. The inner function will close-over the variables of&#xa0;foo&#xa0;before leaving. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">function foo(x) { </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;var tmp = 3; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;return function (y) { </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;alert(x + y + (++tmp)); </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;} </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">} </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">var bar = foo(2); // bar is now a closure.bar(10); </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">The above function will also alert 16, because&#xa0;bar&#xa0;can still refer to&#xa0;x&#xa0;and&#xa0;tmp, even though it is no longer directly inside the scope. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">However, since&#xa0;tmp&#xa0;is still hanging around inside&#xa0;bar's closure, it is also being incremented. It will be incremented each time you call&#xa0;bar. As a result of this it will alert 17 the second time&#xa0;bar(10)&#xa0;is called, 18 the third time, etc. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">(Not for your 6 year old: It is possible to create more than one closure function, either by returning a list of them or by setting them to global variables. All of these will refer to the&#xa0;same&#xa0;x&#xa0;and the same&#xa0;tmp, they don't make their own copies.) </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">Edit: And now to explain the part that isn't obvious. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">Here the number&#xa0;x&#xa0;is a literal number. As with other literals in JavaScript, when&#xa0;foo&#xa0;is called, the number&#xa0;x&#xa0;is&#xa0;copied&#xa0;into&#xa0;foo&#xa0;as its argument&#xa0;x. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">On the other hand, JavaScript always uses references when dealing with Objects. If say, you called&#xa0;foowith an Object, the closure it returns will&#xa0;reference&#xa0;that original Object! </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">function foo(x) { </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;var tmp = 3; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;return function (y) { </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;alert(x + y + tmp); </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;x.memb = x.memb ? x.memb + 1 : 1; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;alert(x.memb);&#xa0; </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">&#xa0;&#xa0;} </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">} </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">var age = new Number(2); </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">var bar = foo(age); // bar is now a closure referencing age. </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3" color="#0000ff">bar(10);</font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">As expected, each call to&#xa0;bar(10)&#xa0;will increment&#xa0;x.memb. What might not be expected, is that&#xa0;x&#xa0;is simply referring to the same object as the&#xa0;age&#xa0;variable! After a couple of calls to&#xa0;bar,&#xa0;age.memb&#xa0;will be 2! </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">This is the basis for memory leaks with HTML objects, but that's a little beyond the scope of this, ahem, article, </font>
    </p>
    <p content="text/html; charset=utf-8" http-equiv="content-type" style="font-variant: normal; margin-left: 0px; margin-bottom: 0; padding-right: 0px; text-transform: none; font-size: 14px; letter-spacing: normal; text-align: left; margin-right: 0px; font-weight: normal; font-family: Arial, Liberation Sans, DejaVu Sans, sans-serif; border-bottom-style: none; padding-bottom: 0px; color: rgb(0, 0, 0); border-left-style: none; vertical-align: baseline; border-top-width: 0px; border-right-style: none; border-left-width: 0px; margin-top: 0px; font-style: normal; background-repeat: repeat; border-top-style: none; white-space: normal; background-position: initial initial; background-color: rgb(255, 255, 255); padding-left: 0px; clear: both; text-indent: 0px; padding-top: 0px; word-spacing: 0px; border-bottom-width: 0px; line-height: 18px; border-right-width: 0px">
      <font size="3">ahem.&#xa0;http://stackoverflow.com/questions/111102#112265</font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="9"/>
</node>
</node>
<node CREATED="1439202381623" ID="ID_725456677" MODIFIED="1439202382664" TEXT="scope"/>
</node>
<node COLOR="#0033ff" CREATED="1419328536293" ID="ID_1341994167" LINK="https://github.com/jsprodotcom/source" MODIFIED="1480681852168" TEXT="repository js pro apps"/>
</node>
<node CREATED="1420822023004" FOLDED="true" ID="ID_470620448" MODIFIED="1480681025590" POSITION="left" TEXT="test">
<node COLOR="#ff0000" CREATED="1420822025631" ID="ID_864695065" MODIFIED="1480677002306" TEXT="mocha"/>
<node COLOR="#ff0000" CREATED="1439382448901" ID="ID_265566764" MODIFIED="1480677002305" TEXT="jasmine"/>
<node COLOR="#338800" CREATED="1480676990039" ID="ID_555716664" LINK="npm/jest.mm" MODIFIED="1480679461626" TEXT="jest"/>
<node COLOR="#ff0000" CREATED="1480676993302" ID="ID_1761679796" MODIFIED="1480677002302" TEXT="chai"/>
</node>
<node CREATED="1319659786877" FOLDED="true" ID="ID_998630532" MODIFIED="1480681003562" POSITION="right" TEXT="Browser">
<node COLOR="#0033ff" CREATED="1364305502013" ID="ID_1392687097" LINK="../OS/Cross/Web/Firefox.mm" MODIFIED="1480679392437" TEXT="firefox">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1357728117546" FOLDED="true" ID="ID_581548053" MODIFIED="1480679393832" TEXT="chrome">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1362497618819" ID="ID_1499538186" MODIFIED="1480679389179" TEXT="xpath">
<node CREATED="1362497636736" ID="ID_183184559" MODIFIED="1480679389537" TEXT="debug">
<node CREATED="1362497640125" ID="ID_529153595" MODIFIED="1362497689346">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      // in console use next
    </p>
    <p>
      <font color="#0033ff">$x('//html/element');</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1357728130460" ID="ID_1879172063" MODIFIED="1420822032471" TEXT="safary">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1357728121553" ID="ID_1753490774" MODIFIED="1420822032471" TEXT="opera">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1357728124219" FOLDED="true" ID="ID_1585452841" MODIFIED="1480679422318" TEXT="ie">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#0033ff" CREATED="1357840628476" FOLDED="true" ID="ID_1214273483" MODIFIED="1480679421913" TEXT="&lt;iframe&gt;">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1480679413700" FOLDED="true" ID="ID_1053492804" MODIFIED="1480679421305" TEXT="tweaks">
<node CREATED="1357840646586" ID="ID_1403734863" MODIFIED="1480679400335">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      if<font color="#ccffcc">&#xa0;</font><b><font color="#0033ff">alert</font></b>&#xa0;occurs before <b>&lt;frame&gt;</b>&#xa0;displayed (even if displaying goes first)
    </p>
    <p>
      -&gt; just move <b><font color="#0033ff">alert</font></b>&#xa0;into <b><font color="#0033ff">function</font></b>&#xa0;and&#xa0; <b><font color="#0033ff">setTimeout( function, 10 )</font></b>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1421312389903" ID="ID_1263606042" LINK="browser hacks &lt;http://browserhacks.com/&gt; " MODIFIED="1421312398387" TEXT="hacks"/>
</node>
<node COLOR="#338800" CREATED="1480676816637" ID="ID_805988520" LINK="mobile.mm" MODIFIED="1480676826708" POSITION="left" TEXT="mobile"/>
</node>
</map>
